#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <map>
#include <string>

#include <EventLoop/Worker.h>
#include <EventLoop/Algorithm.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <TProfile.h>
#include <TLorentzVector.h>
#include <AsgTools/MessageCheck.h>
#include <EventLoopAlgs/AlgSelect.h>
#include "AsgTools/AnaToolHandle.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "JetJvtEfficiency/JetJvtEfficiency.h"
#define GEV 1000
#define MEV 1

class MyxAODAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // job steering;
  bool m_writeOutNTuple;
  bool m_printTruthChain;
  bool doReco;
  bool doTruth;
  BTaggingSelectionTool* btagtool; //!
  CP::MuonSelectionTool m_muonSelection; //!
  CP::IsolationSelectionTool*  iso_tool; //!
  asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle; //!
  AsgElectronLikelihoodTool* m_LooseLH; //!
  AsgPhotonIsEMSelector* m_photonTight; //!
  //Accessors to job steering variables
   TFile *file = new TFile("/afs/cern.ch/user/m/mobelfki/MyxAODAnalysis2/AnalysisCode/MyAnalysis/PtReco/hist-mc15_13TeV.root"); //!
   TProfile* PtReco = (TProfile*) file->Get("PtReco4"); //!
   CP::JetJvtEfficiency *jetsf; //!
public:
  void DoReco(bool v){doReco = v;}
  void DoTruth(bool v){doTruth = v;}
  void PrintTruthChain(bool v){m_printTruthChain = v;}
  void WriteNTuple(bool v){m_writeOutNTuple = v;}
  
  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!

	Double_t pty1; //!
	Double_t pty2; //!
	Double_t phiy1; //!
	Double_t phiy2;	//!
	Double_t phib1; //!
	Double_t phib2; //!
	Double_t ptsl; //!
	Double_t ptl; //!
	Double_t pttl; //!
	Double_t pttsl; //!
	Double_t ptyy; //!
	Double_t dphiyybb; //!
	Double_t dphibb; //!
	Double_t dphiyy; //!
	Double_t dphibbtruth; //!
	Double_t detabb; //!
	Double_t detayy; //!
	Double_t detabbtruth; //!
	Double_t cphiy1; //!
	Double_t cphiy2; //!
	Double_t sphiy1; //!
	Double_t sphiy2; //!
	Double_t cphib1; //!
	Double_t cphib2; //!
	Double_t sphib1; //!
	Double_t sphib2; //!
	Double_t pxyy; //!
	Double_t pyyy; //!
	Double_t pxpy; //!
	Double_t csphib1; //!
	Double_t csphib2; //!
	Double_t etay1; //!
	Double_t etay2; //!
	Double_t etaj2; //!
	Double_t etaj1; //!
	Double_t mbbmu; //!
	Double_t mbbptreco; //!
	Double_t mbbprim; //!
	
  // this is a standard constructor
  MyxAODAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODAnalysis, 1);
  
  // helper functions
public:
  void cTH1F (TH1 **h, std::string name, int nBins, float xMin, float xMax) { *h = new TH1F(name.c_str(), name.c_str(), nBins, xMin, xMax); wk()->addOutput(*h);}
  void cTH2F (TH2 **h, std::string name, int nBins, float xMin, float xMax, int nBinsY, float yMin, float yMax) { *h = new TH2F(name.c_str(), name.c_str(), nBins, xMin, xMax, nBinsY, yMin, yMax); wk()->addOutput(*h);}
  std::vector<TLorentzVector> LeadingAndSub(std::vector<TLorentzVector> jet);
  std::vector<TLorentzVector> LeadingAndSubJet(std::vector<TLorentzVector> jet);
  double getscalefactor(double pt);
};
	
#endif

