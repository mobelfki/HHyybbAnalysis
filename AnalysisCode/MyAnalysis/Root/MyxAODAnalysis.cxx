#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MyAnalysis/MyxAODAnalysis.h>

#include <AsgTools/MessageCheck.h>

#include "PathResolver/PathResolver.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoopAlgs/AlgSelect.h>
#include <TMath.h>
#include <TTree.h>
#include <TH1.h>
#include <TH3.h>
#include <TF1.h>
#include <TVector2.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TProfile.h>
#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODMissingET/MissingET.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODBTagging/BTagging.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "PathResolver/PathResolver.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "xAODTracking/Vertex.h"

Double_t pi=TMath::Pi();

// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODAnalysis)



MyxAODAnalysis :: MyxAODAnalysis () : m_writeOutNTuple(false), m_printTruthChain(true), doReco(false), doTruth(true)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  
  //m_triggerChainGroupString.push_back("L1_2EM3");
}



EL::StatusCode MyxAODAnalysis :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  job.useXAOD ();
  ANA_CHECK(xAOD::Init());
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  
 	//histogramme
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: changeInput (bool /* firstFile */)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  // ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
 
  xAOD::TEvent* event = wk()->xaodEvent();
  Info("initialize()", "Number of events to process = %lli", event->getEntries() ); // print long long int
  
	//add branches to output NTuple
	if(m_writeOutNTuple){
		EL::NTupleSvc *ntuple = EL::getNTupleSvc (wk(), "output");
		ntuple->tree()->Branch("pty1", &pty1);
		ntuple->tree()->Branch("pty2", &pty2);
		ntuple->tree()->Branch("phiy1", &phiy1);
		ntuple->tree()->Branch("phiy2", &phiy2);
		ntuple->tree()->Branch("phij1", &phib1);
		ntuple->tree()->Branch("phij2", &phib2);
		ntuple->tree()->Branch("ptj1", &ptsl);
		ntuple->tree()->Branch("ptj2", &ptl);
		ntuple->tree()->Branch("pttl", &pttl);
		ntuple->tree()->Branch("pttsl", &pttsl);
		ntuple->tree()->Branch("ptyy", &ptyy);
		ntuple->tree()->Branch("dphiyybb", &dphiyybb);
		ntuple->tree()->Branch("dphibb", &dphibb);
		ntuple->tree()->Branch("dphiyy", &dphiyy);
		ntuple->tree()->Branch("dphibbtruth", &dphibbtruth);
		ntuple->tree()->Branch("detabb", &detabb);
		ntuple->tree()->Branch("detayy", &detayy);
		ntuple->tree()->Branch("detabbtruth", &detabbtruth);
		ntuple->tree()->Branch("etaj1", &etaj1);
		ntuple->tree()->Branch("etaj2", &etaj2);
		ntuple->tree()->Branch("etay1", &etay1);
		ntuple->tree()->Branch("etay2", &etay2);
		
	//
		ntuple->tree()->Branch("cphiy1", &cphiy1);
		ntuple->tree()->Branch("cphiy2", &cphiy2);
		ntuple->tree()->Branch("sphiy1", &sphiy1);
		ntuple->tree()->Branch("sphiy2", &sphiy2);
		ntuple->tree()->Branch("cphib1", &cphib1);
		ntuple->tree()->Branch("cphib2", &cphib2);
		ntuple->tree()->Branch("sphib1", &sphib1);
		ntuple->tree()->Branch("sphib2", &sphib2);
		ntuple->tree()->Branch("pxyy", &pxyy);
		ntuple->tree()->Branch("pyyy", &pyyy);
		ntuple->tree()->Branch("pxpy", &pxpy);
		ntuple->tree()->Branch("csphib1", &csphib1);
		ntuple->tree()->Branch("csphib2", &csphib2);
	}
  btagtool = new BTaggingSelectionTool("BTaggingSelectionTool");
  btagtool->setProperty("MaxEta", 2.5);
  btagtool->setProperty("MinPt", 20000.);
  btagtool->setProperty("JetAuthor","AntiKt4EMTopoJets");
  btagtool->setProperty("TaggerName", "MV2c10");
  btagtool->setProperty("FlvTagCutDefinitionsFileName", "/afs/cern.ch/atlas/www/GROUPS/DATABASE/GroupData/xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-06-07_v2.root");
  btagtool->setProperty("OperatingPoint", "FixedCutBEff_70");
  btagtool->initialize();

	//m_muonSelection.msg().setLevel( MSG::VERBOSE );
        m_muonSelection.setProperty( "MaxEta", 2.5 );
        m_muonSelection.setProperty( "MuQuality", 0);
	m_muonSelection.setProperty("TurnOffMomCorr",true);
        m_muonSelection.initialize();

	//Jet Calibration
	 const std::string name = "MyxAODAnalysis"; 
  	 TString jetAlgo = "AntiKt4EMTopo" ; 
  	 TString config = "JES_data2016_data2015_Recommendation_Dec2016.config"; 
  	 TString calibSeq = "JetArea_Residual_Origin_EtaJES_GSC"; 
	 TString calibArea =  "00-04-76";
 	 bool isData = false; 

//  asg::AnaToolHandle<IJetCalibrationTool> JetCalibrationTool_handle;
	//JetCalibrationTool_handle = new  asg::AnaToolHandle<IJetCalibrationTool>();
  JetCalibrationTool_handle.setTypeAndName("JetCalibrationTool/MyxAODAnalysis");
  if( !JetCalibrationTool_handle.isUserConfigured() ){
    ANA_CHECK( ASG_MAKE_ANA_TOOL(JetCalibrationTool_handle, JetCalibrationTool) );
    ANA_CHECK( JetCalibrationTool_handle.setProperty("JetCollection",jetAlgo.Data()) );
    ANA_CHECK( JetCalibrationTool_handle.setProperty("ConfigFile",config.Data()) );
    ANA_CHECK( JetCalibrationTool_handle.setProperty("CalibSequence",calibSeq.Data()) );
    ANA_CHECK( JetCalibrationTool_handle.setProperty("IsData",isData) );
    ANA_CHECK( JetCalibrationTool_handle.retrieve() );
  }

// Electron Identification 
	m_LooseLH = new AsgElectronLikelihoodTool ("LooseLH");
	m_LooseLH->setProperty("WorkingPoint", "LooseLHElectron");
	m_LooseLH->setProperty("ConfigFile", "/afs/cern.ch/atlas/www/GROUPS/DATABASE/GroupData/ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodLooseOfflineConfig2017_Smooth.conf");
	m_LooseLH->initialize();

//Photon Identification 
	m_photonTight = new AsgPhotonIsEMSelector ("PhotonTightIsEMSelector");
	m_photonTight->setProperty("isEMMask",egammaPID::PhotonTight);
	m_photonTight->setProperty("ConfigFile","/afs/cern.ch/atlas/www/GROUPS/DATABASE/GroupData/ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMTightSelectorCutDefs.conf");
	if(!m_photonTight->initialize().isSuccess()){
		Fatal("MyFunction", "Failed to initialize PhotonTightIsEMSelector");
	}

//Photon Isolation 
	iso_tool = new CP::IsolationSelectionTool("iso_tool");
 	//iso_tool->setProperty("muonWP","Loose");
  	iso_tool->setProperty("PhotonWP","FixedCutLoose");
	iso_tool->initialize();

//JVT 

	jetsf = new CP::JetJvtEfficiency("jetsf");
	jetsf->setProperty("WorkingPoint","Medium");
	jetsf->setProperty("SFFile","/afs/cern.ch/atlas/www/GROUPS/DATABASE/GroupData/JetJvtEfficiency/Moriond2017/JvtSFFile_EM.root");
	assert(jetsf->initialize());

//Histo

	book (TH1F ("h_TruthPhotonPt", "h_TruthPhotonPt", 100, 0, 500)); 
	book (TH1F ("h_TruthPhotonE", "h_TruthPhotonE", 100, 0, 500)); 
	book (TH1F ("h_TruthPhotonEta", "h_TruthPhotonEta", 100, -5, 5));
	book (TH1F ("h_TruthPhotonPhi", "h_TruthPhotonPhi", 100, -4, 4));
	book (TH1F ("h_TruthPhotonN", "h_TruthPhotonN", 10, -0.5, 9.5)); 
	book (TH1F ("h_TruthPhotonInvM", "h_TruthPhotonInvM", 100, 0, 250)); 
	book (TH1F ("h_TruthPhotonDeltaPhi", "h_TruthPhotonDeltaPhi", 100, 0, 5));
	book (TH1F ("h_TruthJetPt", "h_TruthJetPt", 200, 0, 500)); 
	book (TH1F ("h_TruthJetM", "h_TruthJetM", 100, 0, 500));
	book (TH1F ("h_TruthJetEta", "h_TruthJetEta", 100, -6, 6)); 
	book (TH1F ("h_TruthJetPhi", "h_TruthJetPhi", 100, -4, 4)); 
	book (TH1F ("h_TruthJetN", "h_TruthJetN", 16, -0.5, 15.5)); 
	book (TH1F ("h_TruthJetNoCountN", "h_TruthJetNoCountN", 16, -0.5, 15.5)); 
	book (TH1F ("h_TruthJetNoCountPt", "h_TruthJetNoCountPt", 200, 0, 500)); 
	book (TH1F ("h_DiH_Px","h_DiH_Px", 50, -500, 500));
	book (TH1F ("h_TruthJetInvM", "h_TruthJetInvM", 100, 0, 250));
	book (TH1F ("h_TruthParticleStatus","h_TruthParticleStatus", 10, -20.5, 20));
	book (TH1F ("h_TruthBQuarkPt", "h_TruthBQuarkPt", 200, 0, 500)); 
	book (TH1F ("h_TruthBQuarkM", "h_TruthBQuarkM", 100, 0, 500));
	book (TH1F ("h_TruthBQuarkEta", "h_TruthBQuarkEta", 100, -6, 6)); 
	book (TH1F ("h_TruthBQuarkPhi", "h_TruthBQuarkPhi", 100, -4, 4));
	book (TH1F ("h_TruthBQuarkN","h_TruthBQuarkN", 10, -0.5, 9.5));
	book (TH1F ("h_TruthBQuarkInvM","h_TruthBQuarkInvM", 100, 0, 500));
	book (TH1F ("h_TruthBQuarkDeltaPhi","di-B DeltaPhi", 100, -4, 4));
	book (TH1F ("h_TruthDiH_Pt","h_TruthDiH_Pt", 50, 0, 300));
	book (TH1F ("h_TruthDiH_Aco","h_TruthDiH_Aco", 100, 0, 1));
	book (TH1F ("h_DiH_Jet_DeltaPhi","h_DiH_Jet_DeltaPhi", 100, -3.15,3.15));
	book (TH1F ("h_TruthDiH_PtNorm","Norm de la somme Vectorielle de Pt des H", 100, 0, 250));
	book (TH1F ("h_TruthDiH_PNorm","Norm de la somme Vectorielle de P des H", 100, 0, 250));
	book (TH1F ("h_TruthJetBDeltaE", "Delta E entre le quark B et Jet associe", 100, -100, 100));
	book (TH1F ("h_TruthJetBDeltaR", "Delta dR entre le quark B et Jet", 100, 0, 3));
	book (TH1F ("h_NsmatchJet","Number of Matched Jet", 10, -0.5, 9.5));
	book (TH1F ("h_NsmatchBQ","Number of non Matched Quark", 10, -0.5, 9.5));
	book (TH1F ("h_TruthSmJetInvM", "h_TruthSmJetInvM", 100, 0, 250));
	book (TH1F ("h_NofMuonInCone","h_NofMuonInCone", 10, -0.5, 9.5));
	book (TH1F ("h_NofMuonAddEvt","h_NofMuonAddEvt", 10, -0.5, 9.5));
	book (TH1F ("h_TruthJetInvAfMuonCorr","h_TruthJetInvAfMuonCorr",30, 50, 200));
	book (TH1F ("h_TruthBQuarknSmtPt", "Pt Of B-Quark not Matched", 100, 0, 300));
	book (TH1F ("h_TruthBQuarknSmtEta", "#eta Of B-Quark not Matched", 100, -6, 6));
	book (TH1F ("h_TruthMJetMass", "Mass of Matched jet", 100, 0, 100));
	book (TH1F ("h_TruthJetInvAfMETCorr","h_TruthJetInvAfMETCorr",100, 0, 250));
	book (TH1F ("h_Ptyybb","Ptyybb", 100, -100, 100));
	book (TH1F ("h_Ey","Resulotion of #gamma energy", 100, -100, 100));
	book (TH1F ("h_Pt1","Resulotion", 200, -1.02, 1.5));
	book (TH2F ("h_Pt2","Resulotion VS PT",200,-1.02,1.5,100,5,500));
	book (TH2F ("h_Pt3","Pt_{Jet} VS Pt_{B-Quark}",100,0,500,100,0,500));
	book (TH1F ("h_Phi1","Resulotion of Jet1 #phi",100,-3.15,3.15));
	book (TH1F ("h_Phi2","Resulotion of Jet2 #phi",100,-3.15,3.15));
	book (TH1F ("h_N","Numbre of Jet associed to bb",10,-0.5,9.5));
	book (TH1F ("h_TruthDiH_Ptc","h_TruthDiH_Ptc", 100, 0, 250));
	book (TH1F ("h_RecoJetPt", "h_RecoJetPt", 200, 5, 500)); 
	book (TH1F ("h_RecoJetM", "h_RecoJetM", 100, 0, 500));
	book (TH1F ("h_RecoJetEta", "h_RecoJetEta", 100, -6, 6)); 
	book (TH1F ("h_RecoJetPhi", "h_RecoJetPhi", 100, -4, 4)); 
	book (TH1F ("h_RecoJetN", "h_RecoJetN", 10, -0.5, 9.5)); 
	book (TH1F ("h_RecoBJetPt", "h_RecoBJetPt", 200, 5, 500)); 
	book (TH1F ("h_RecoBJetM", "h_RecoBJetM", 100, 0, 500));
	book (TH1F ("h_RecoBJetEta", "h_RecoBJetEta", 100, -6, 6)); 
	book (TH1F ("h_RecoBJetPhi", "h_RecoBJetPhi", 100, -4, 4)); 
	book (TH1F ("h_RecoBJetN", "h_RecoBJetN", 10, -0.5, 9.5)); 
  	book (TH1F ("h_PtRJ","Resulotion with Truth Leading Jet",200,-0.7,0.7));
	book (TH1F ("h_PtRJ2","Resulotion with Truth SubLeading Jet",200,-0.7,0.7));
	book (TH1F ("h_PtRQ","Resulotion with Truth Leading Quark",200,-0.7,0.7));
	book (TH1F ("h_PtRQ2","Resulotion with Truth SubLeading Quark",200,-0.7,0.7));
	book (TH1F ("h_InvJet", "M_{jj}", 100, 0, 250));
	book (TH1F ("h_InvPhoton", "M_{#gamma#gamma}", 100, 0, 250));
	book (TH1F ("h_RecoPhotonN", "h_RecoPhotonN", 16, -0.5, 15.5)); 
        book (TH1F ("h_PtRY","Resulotion of Truth Leading Photon",200,-0.5,0.5));
	book (TH1F ("h_PtRY2","Resulotion of Truth SubLeading Photon",200,-0.5,0.5));
        book (TH1F ("h_DiH_Pt","h_DiH_Pt", 100, 0, 300));
	book (TH1F ("h_Ptyy","Pt_{#gamma#gamma}",100,0,300));
	book (TH1F ("h_Ptbb","Pt_{bb}",100,0,300));
	book (TH1F ("h_TruthDiH_Px","h_TruthDiH_Px", 50, -500, 500));
	book (TH1F ("h_TruthDiH_Py","h_TruthDiH_Py", 50, -500, 500));
	book (TH2F ("h_TruthDiH_PxPy","h_TruthDiH_PxPy",200,-500, 500,200,-500,500));
	book (TH1F ("h_TruthDiH_PxPysum","sum of Px Py of HH", 200, -500,500));
	book (TH1F ("h_RecoBJetCalPt", "h_RecoBJetCalPt", 200, 5, 500)); 
	book (TH1F ("h_RecoBJetCalM", "h_RecoBJetCalM", 100, 0, 500));
	book (TH1F ("h_RecoBJetCalEta", "h_RecoBJetCalEta", 100, -6, 6)); 
	book (TH1F ("h_RecoBJetCalPhi", "h_RecoBJetCalPhi", 100, -4, 4)); 
	book (TH1F ("h_RecoBJetCalN", "h_RecoBJetCalN", 10, -0.5, 9.5));
	book (TH1F ("h_RecoNJet_additional", "h_RecoNJet_additional", 10, -0.5, 9.5));
	book (TH1F ("h_InvJetCal", "Calibrated M_{jj}", 30, 50, 200));
	book (TH1F ("h_InvJetAfterMuon", "M_{jj}", 30, 50, 200));
	book (TH1F ("h_NumberMuonAdd","Number of muon added",10,-0.5,9.5));
	//book (TH1F ("h_InvJetAfterMET", "M_{jj}", 100, 0, 250));
	book (TH1F ("h_Phiy","Resolution on #phi_{#gamma}",25,-0.12,0.12));
	book (TH1F ("h_Phib","Resolution on #phi_{b-jet}",25,-0.2,0.2));
	book (TH1F ("h_PtR","Resolution with Truth Leading Quark",200,-0.7,0.7));
	book (TH1F ("h_PtR1","Resolution with Truth Leading Quark",200,-0.7,0.7));
	book (TH1F ("h_PtR2","Resolution with Truth Leading Jet",200,-0.7,0.7));
	book (TH1F ("h_Etab","Resolution on #eta_{b-jet}",200,-1,1));
	book (TH2F ("h_RvsPtQ","Pt_{reco} - Pt_{B} VS Pt_{B}",100,0,300,100,-0.5,0.5));
	book (TH2F ("h_RvsPtQ2","Pt_{reco} - Pt_{B} VS Pt_{B} after shift",100,0,300,100,-0.5,0.5));
	book (TH1F ("h_NumberofWZBJet","Number of WZ B-Jet",10,-0.5,9.5));
	book (TH1F ("h_NumberofmatchedWZBJet","Number of matched WZ B-Jet",10,-0.5,9.5));
	book (TH1F ("h_InvWZJet", "WZ M_{jj}", 30, 50, 200));
	book (TProfile ("PtReco0","Profile of PtReco correction versus Pt reco case of zero muons",300,0,300,0.8,1.2));
	book (TProfile ("PtReco1","Profile of PtReco correction versus Pt reco case of more one muons",300,0,300,0.8,1.2));
	book (TH1F ("h_factor","PtReco Factor Scale",300,0,1.5));
	book (TH1F ("h_InvAfterPtReco","Invariant Mass After PtReco Correction", 30 , 50, 200));
	book (TH1F ("h_NumberElecAdd","Number of Electron added",10,-0.5,9.5));
	book (TProfile ("PtReco2","Profile of PtReco correction versus Pt reco case of TruthJet",300,0,300,0.8,1.2));
	book (TH1F ("h_factor2","PtReco Factor Scale Truth Jet",300,0,1.5));
	book (TH1F ("h_InvAfterPtReco2","Invariant Mass After PtReco Correction Truth Jet", 30 , 50, 200));
	book (TH1F ("h_PtWZvsPttruth", " Pt_{truth} - Pt_{TruthWZ}", 100,-5,5));
	book (TProfile  ("h_PtQvsPttruth","Pt_{truth} - Pt_{Quarks}/Pt_{Quarks} vs Pt_{Quarks}",100,0,500,-300,300));
	book (TProfile  ("h_PtQvsPtreco","Pt_{reco} - Pt_{Quarks}/Pt_{Quarks} vs Pt_{reco}",100,0,500,-300,300));
	book (TProfile  ("h_PtQvsPtreco2","Pt_{reco} - Pt_{Quarks}/Pt_{Quarks} vs Pt_{Quarks}",100,0,500,-300,300));
	book (TProfile ("PtReco3","Profile of PtReco correction versus Pt reco case of all Jets",300,0,300,0.8,1.2));
	book (TProfile ("PtReco4","Profile of PtReco correction versus Pt reco case of Quarks",50,0,500,0.8,1.2));
	book (TH1F ("h_InvAfterPtReco3","Invariant Mass After PtReco Correction", 30 , 50, 200));
	book (TH1F ("h_TruthDiH_PxAfter","h_TruthDiH_Px after selection", 50, -500, 500));
	book (TH1F ("h_TruthDiH_PyAfter","h_TruthDiH_Py after selection", 50, -500, 500));
	book (TH1F ("h_RecoDiH_Px","h_RecoDiH_Px 2 tag 2 photon", 50, -500, 500));
	book (TH1F ("h_RecoDiH_Py","h_RecoDiH_Py 2 tag 2 photon", 50, -500, 500));
	book (TH1F ("h_DemiRecoDiH_Px","h_RecoDiH_Px 2 tag 2 photon truth", 50, -500, 500));
	book (TH1F ("h_DemiRecoDiH_Py","h_RecoDiH_Py 2 tag 2 photon truth", 50, -500, 500));
	book (TH1F ("h_RecoDiH_Pt","h_RecoDiH_Pt", 50, 0, 500));
	book (TH1F ("h_RecoDiy_Pt","h_RecoDiy_Pt", 50, 0, 500));
	book (TH1F ("h_RecoDiy_Px","h_RecoDiy_Px", 100, -500, 500));
	book (TH1F ("h_RecoDiy_Py","h_RecoDiy_Py", 100, -500, 500));
	book (TH1F ("h_AfterDiH_Pt","h_AfterDiH_Pt", 50, 0, 500));
	book (TProfile ("PtEtaJet","PtEtaJetReco",100,0,300,-2.7,2.7));
	book (TProfile ("PtEtaJet1","PtEtaJetReco",100,0,300,-2.7,2.7));
	book (TH2F ("h_PxRvsPxTr","Px_{HH} Reco vs Px_{HH} Truth",100,-500, 500,100,-500,500));
	book (TH2F ("h_PxRvsPxTr2","Px_{H#rightarrow#gamma#gamma} Reco vs Px_{H#rightarrow#gamma#gamma} Truth",100,-500, 500,100,-500,500));
	book (TH2F ("h_PxRvsPxTr3","Px_{H#rightarrow bb} Reco vs Px_{H#rightarrow bb} Truth",100,-500, 500,100,-500,500));
	book (TH2F ("h_PxRvsPxTr4","Px_{H#rightarrow bb} vs -Px_{H#rightarrow#gamma#gamma} Truth",100,-500, 500,100,-500,500));
	book (TH2F ("h_PxHHvsPxJet","Px_{HH} vs Px_{Jets} Truth",100,-500, 500,100,-500,500));
	book (TH1F ("DeltaRjj"," Delta R jj",50,0,10));
	book (TH1F ("DeltaEtajj"," Delta Eta jj",50,0,4.5));
	book (TH1F ("Acojj"," Acoplanarity jj",50,0,1));
	book (TH1F ("mbb","M_{bb}", 30 , 50, 200));
	book (TH1F ("myybb","M_{yybb}", 30 , 200, 1000));
	book (TH1F ("mbbAfterCut","M_{bb}", 10 , 90, 140));
	book (TH1F ("h_ptresponce040","Pt responce 0-40",100,0.,2.));
	book (TH1F ("h_ptresponce4050","Pt responce 40-50",100,0.,2.));
	book (TH1F ("h_ptresponce5060","Pt responce 50-60",100,0.,2.));
	book (TH1F ("h_ptresponce6070","Pt responce 60-70",100,0.,2.));
	book (TH1F ("h_ptresponce7080","Pt responce 70-80",100,0.,2.));
	book (TH1F ("h_ptresponce80100","Pt responce 80-100",100,0.,2.));
	book (TH1F ("h_ptresponce100","Pt responce +100",100,0.,2.));
	book (TH1F ("h_mbb2jet2btagJES","mbb 2jet 2tag JES",30,50,200));
	book (TH1F ("h_mbb2jet2btagMuon","mbb 2jet 2tag Muon",30,50,200));
	book (TH1F ("h_mbb2jet2btagPtReco","mbb 2jet 2tag PtReco",30,50,200));
	book (TH1F ("h_mbb3jet2btagJES","mbb 3jet 2tag JES",30,50,200));
	book (TH1F ("h_mbb3jet2btagMuon","mbb 3jet 2tag Muon",30,50,200));
	book (TH1F ("h_mbb3jet2btagPtReco","mbb 3jet 2tag PtReco",30,50,200));
	book (TProfile ("mass","Profile of invariant mass correction ",30,50,200,0.8,1.2));
	book (TH1F ("h_mbb_sm","mbb",30,50,200));
	book (TH1F ("JVF","JVF",100,0,1));
	book (TH1F ("JVT","JVT",100,-1,1));
	book (TH1F ("JVTRPT","JVTRPT",100,0,1));
	book (TH1F ("eta0jvf", " eta des jets avec jvf == 0",100,-6,6));
	book (TH2F ("JVFvsRPT"," JVF vs JVTRPT",100,0,1,100,0,1));
	book (TH2F ("PxHHvsPxJet","",100,-300,300,100,-300,300));
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
	xAOD::TStore store;
  	TVector2 som2hh;
	TVector3 som3hh;
	TLorentzVector p,j,t,b,h1,h2,tm,sm,sm0,sm1,sm2,mt,y,alljet,allrecojet,bb,jj,jjbb,bj,ph,sysyy,sysbb,sysyybb,muon,bjet1plusmuon,bjet2plusmuon,jmet, bjet1plusmet,bjet2plusmet,bjcal,test,wzj,test2,test3,ele,bjet1pluselectron,bjet2pluselectron,test4,test5,test6;
	std::vector<TLorentzVector> photon,jet,tparticle,bs,ys,hs,sjet,tmuon,sum,summ,nsb,nsjet,mtv,alljets,allrecojets,bjet,LS_truthjet,LS_recojet,LS_truthQuark,recophoton,LS_recophoton, LS_truthphoton,recomuon,jetmet,bjetcal,wzjet,matchedwzjet, LS_truthWZJet, scaledjet,scaledjet2,electron,scaledjet3,jetnotcount;
	
	// variable Cleaning
		ptl = 0, ptsl = 0, phib1 = 0, phib2 = 0, phiy1 = 0, phiy2 = 0, pty1 = 0, pty2 = 0, pttl = 0, pttsl = 0, ptyy = 0, dphiyybb = 0, dphibb = 0, dphibbtruth = 0, detabb = 0, detabbtruth = 0;
		cphiy1 = 0, cphiy2 = 0, sphiy1 = 0, sphiy2 = 0, cphib1 = 0, cphib2 = 0, sphib1 = 0, sphib2 = 0;
		pxyy = 0, pyyy = 0 , pxpy = 0, csphib1 = 0, csphib2 = 0, dphiyy = 0, detayy = 0;	

  	xAOD::TEvent* event = wk()->xaodEvent();
  	const xAOD::EventInfo *eventInfo = nullptr;
  	ANA_CHECK(event->retrieve( eventInfo, "EventInfo"));
	const DataVector<xAOD::Muon> *recoMuonVector = nullptr;
	ANA_CHECK(event->retrieve( recoMuonVector, "Muons"));
	xAOD::Muon::Quality muon_quality;
	const DataVector<xAOD::Electron> *recoElectronVector = nullptr;
	ANA_CHECK(event->retrieve( recoElectronVector, "Electrons")); 
	const DataVector<xAOD::Photon> *recoPhotonVector = nullptr;
	ANA_CHECK(event->retrieve( recoPhotonVector, "Photons" ));
 	const DataVector<xAOD::TruthParticle> *truthPhotonVector = nullptr, *truthParticleVector = nullptr;
    	//ANA_CHECK (event->retrieve( truthPhotonVector, "egammaTruthParticles" ));
	ANA_CHECK (event->retrieve( truthParticleVector, "TruthParticles" ));
	const DataVector<xAOD::Jet> *truthJetVector = nullptr, *recoJetVector = nullptr, *truthWZJetVector = nullptr;
	ANA_CHECK (event->retrieve ( truthJetVector, "AntiKt4TruthJets" )); 
	ANA_CHECK (event->retrieve ( recoJetVector, "AntiKt4EMTopoJets" ));
	ANA_CHECK (event->retrieve ( truthWZJetVector, "AntiKt4TruthWZJets" ));
	const xAOD::MissingETContainer *MET = nullptr, *RefMET = nullptr;
	ANA_CHECK (event->retrieve ( MET, "MET_Truth"));
	ANA_CHECK (event->retrieve ( RefMET, "MET_Reference_AntiKt4EMTopo"));
	//const xAOD::MissingET* met_jet = (*RefMET)["RefJet"];
	const xAOD::TruthEventContainer* xtruth = NULL;
	ANA_CHECK( event->retrieve( xtruth, "TruthEvents"));
	xAOD::TruthEventContainer::const_iterator itr;
	for(itr = xtruth->begin(); itr!=xtruth->end(); ++itr){
	const xAOD::TruthParticle* particle = NULL;
	const xAOD::TruthVertex* dVtx = NULL;
	bool truth = false;
	//int nv = (*itr)->nTruthVertices();
if(truth){
	int np = (*itr)->nTruthParticles();
		for(int i = 0; i<np; i++){
				particle = (*itr)->truthParticle(i);
			if(particle != NULL){
				bool decay = particle->hasDecayVtx();
				int id = particle->pdgId();
				if(particle->absPdgId() == 13){
					tm.SetPxPyPzE(particle->px()*0.001,particle->py()*0.001,particle->pz()*0.001,particle->e()*0.001);
					tmuon.push_back(tm);
				}
				if(decay && id == 25){
					bool ghiggs = false, bhiggs = false;
					int nb = 0, ng = 0;
					//std::cout<<"Higgs Decay is founded"<<std::endl;
					dVtx = particle->decayVtx();
					int nop = dVtx->nOutgoingParticles(); 
					for(int j = 0; j<nop; j++){
					  	const xAOD::TruthParticle* prodparticle = dVtx->outgoingParticle(j);
						int prodid = fabs(prodparticle->pdgId());
						if(prodid == 5 && (nb <2)){
							//std::cout<<"B form Higgs is founded"<<std::endl;
							b.SetPxPyPzE(prodparticle->px()*0.001,prodparticle->py()*0.001,prodparticle->pz()*0.001,prodparticle->e()*0.001);
							bs.push_back(b);
							bhiggs = true;
							nb++;
							hist("h_TruthBQuarkPt")->Fill (b.Pt()); 
							hist("h_TruthBQuarkM")->Fill (b.M()); 
							hist("h_TruthBQuarkEta")->Fill (b.Eta());
							hist("h_TruthBQuarkPhi")->Fill (b.Phi());
						}
					//	if((particle->pt()*0.001) > 150){
						if(prodid == 22 && (ng<2)){
							//photon form higgs is founded
							y.SetPxPyPzE(prodparticle->px()*0.001,prodparticle->py()*0.001,prodparticle->pz()*0.001,prodparticle->e()*0.001);
							ys.push_back(y);
							ghiggs = true;
							ng++;
						//}
					}}
					if(bhiggs && (nb == 2)){
					h1.SetPxPyPzE(particle->px()*0.001,particle->py()*0.001,particle->pz()*0.001,particle->e()*0.001);
					hs.push_back(h1);}
					if(ghiggs && (ng == 2)){
					h2.SetPxPyPzE(particle->px()*0.001,particle->py()*0.001,particle->pz()*0.001,particle->e()*0.001);
					hs.push_back(h2);}	
				}
			}			
		}
	
		//som2hh.Set((hsb[0].Px()+hsg[1].Py()),(hsb[0].Px()+hsg[1].Py()));
		//som3hh.SetXYZ((hsb[0].Px()+hsg[1].Py()),(hsb[0].Px()+hsg[1].Py()),(hsb[0].Pz()+hsg[1].Pz()));
		//float norm2 = sqrt(pow(som2hh.Px(),2)+pow(som2hh.Py(),2));
		//float norm3 = sqrt(pow(som3hh.X(),2)+pow(som3hh.Y(),2)+pow(som3hh.Z(),2));
		
		
		float ptg1 = ys[0].Pt(); float ptg2 = ys[1].Pt(); float ptb1 = bs[0].Pt(); float ptb2 = bs[1].Pt();
		float cg1 = cos(ys[0].Phi()); float cg2 = cos(ys[1].Phi()); float cb1 = cos(bs[0].Phi()); float cb2 = cos(bs[1].Phi());
		float sg1 = sin(ys[0].Phi()); float sg2 = sin(ys[1].Phi()); float sb1 = sin(bs[0].Phi()); float sb2 = sin(bs[1].Phi());
		float dptc = sqrt( pow((((ptg1*cg1)+(ptg2*cg2))+((ptb1*cb1)+(ptb2*cb2))),2) + pow((((ptg1*sg1)+(ptg2*sg2))+((ptb1*sb1)+(ptb2*sb2))),2) );
		//float aco = 1 - (fabs((hs[0]).DeltaPhi(hs[1]))/pi);
		float dpt = fabs((hs[0]).Pt() - (hs[1]).Pt());
		
		float aco = 1 - (fabs((hs[0]).DeltaPhi(hs[1]))/pi);
		float pthh = (hs[0] + hs[1]).Pt();
		float pxhh = (hs[0] + hs[1]).Px();
		float pyhh = (hs[0] + hs[1]).Py();
		
		float norm2 = sqrt(pow(hs[0].Px()+hs[1].Px(),2) + pow(hs[0].Py()+hs[1].Py(),2));
		float norm3 = sqrt(pow(hs[0].Px()+hs[1].Px(),2) + pow(hs[0].Py()+hs[1].Py(),2) + pow(hs[0].Pz()+hs[1].Pz(),2));
		hist("h_TruthDiH_PtNorm")->Fill(norm2);
		hist("h_TruthDiH_PNorm")->Fill(norm3);
		hist("h_TruthDiH_Aco")->Fill(aco);
		
		hist("h_TruthDiH_Aco")->Fill(aco);
		hist("h_TruthDiH_Pt")->Fill(pthh);
		//if((cg1 == sg1) && (cg2 == sg2) && (cb1 == sb1) && (cb2 == sb2)){
		hist("h_TruthDiH_Px")->Fill(pxhh);
		hist("h_TruthDiH_Py")->Fill(pyhh);
		//}
		
		hist("h_TruthDiH_PxPy")->Fill(pxhh,pyhh);
		hist("h_TruthDiH_PxPysum")->Fill(pxhh+pyhh);
		hist("h_TruthDiH_Ptc")->Fill(dptc);
		hist("h_TruthBQuarkN")->Fill(bs.size());
		hist("h_TruthBQuarkInvM")->Fill((bs[0]+bs[1]).M());
		hist("h_TruthBQuarkDeltaPhi")->Fill(bs[0].DeltaPhi(bs[1]));
		hist("h_DiH_Pt")->Fill(sqrt(pow((ys[0]+ys[1]).Pt(),2) + pow(ptb1-ptb2,2) + 4*ptb1*ptb2*pow(cos(bs[0].DeltaPhi(bs[1])),2) + 2*((ys[0]+ys[1]).Pt())*sqrt(pow(ptb1-ptb2,2) + 4*ptb1*ptb2*pow(cos(bs[0].DeltaPhi(bs[1])),2))*cos((ys[0]+ys[1]).DeltaPhi(bs[0]+bs[1]))));
		//Truth Photo kinematic
	for(unsigned int i = 0; i < truthPhotonVector->size(); i++){
	if( fabs((truthPhotonVector->get(i))->eta()) < 1.37 || ( fabs((truthPhotonVector->get(i))->eta()) > 1.5 && fabs((truthPhotonVector->get(i))->eta()) < 2.37 ) ){
		//if((truthPhotonVector->get(i))->pdgid() == 22){		 
		//hist("h_TruthPhotonPt")->Fill (((truthPhotonVector->get(i))->pt())*0.001); 
		hist("h_TruthPhotonE")->Fill (((truthPhotonVector->get(i))->e())*0.001); 
		hist("h_TruthPhotonEta")->Fill (((truthPhotonVector->get(i))->eta())); 
		p.SetPxPyPzE(((truthPhotonVector->get(i))->px())*0.001,((truthPhotonVector->get(i))->py())*0.001,((truthPhotonVector->get(i))->pz())*0.001,((truthPhotonVector->get(i))->e())*0.001);	
		hist("h_TruthPhotonPhi")->Fill(p.Phi());
		photon.push_back(p);
	}}
		//Truth WZ Jet Kinematic
	for(unsigned int i = 0; i < truthWZJetVector->size(); i++){
		int id = (truthWZJetVector->get(i))->auxdata<int>("ConeTruthLabelID");
			if( id == 5){
				wzj.SetPtEtaPhiM(((truthWZJetVector->get(i))->pt())*0.001,((truthWZJetVector->get(i))->eta()),((truthWZJetVector->get(i))->phi()),((truthWZJetVector->get(i))->m())*0.001);
				wzjet.push_back(wzj);
			}
	}
		hist("h_NumberofWZBJet")->Fill(wzjet.size());
		//Truth Jet kinematic
	for(unsigned int i = 0; i < truthJetVector->size(); i++){	
	int id = (truthJetVector->get(i))->auxdata<int>("ConeTruthLabelID");
	if(((truthJetVector->get(i))->pt())*0.001 > 30.){
	//int partid = (truthJetVector->get(i))->auxdata<int>("PartonTruthLabelID");
	//int ghosthiggs = (truthJetVector->get(i))->auxdata<int>("GhostHBosonsCount");
		alljet.SetPtEtaPhiM(((truthJetVector->get(i))->pt())*0.001,((truthJetVector->get(i))->eta()),((truthJetVector->get(i))->phi()),((truthJetVector->get(i))->m())*0.001);
		alljets.push_back(alljet);
	if( id == 5){	 
		//hist("h_TruthJetPt")->Fill (((truthJetVector->get(i))->pt())*0.001); 
		hist("h_TruthJetM")->Fill (((truthJetVector->get(i))->m())*0.001); 
		hist("h_TruthJetEta")->Fill (((truthJetVector->get(i))->eta()));
		hist("h_TruthJetPhi")->Fill (((truthJetVector->get(i))->phi())); 
		j.SetPtEtaPhiM(((truthJetVector->get(i))->pt())*0.001,((truthJetVector->get(i))->eta()),((truthJetVector->get(i))->phi()),((truthJetVector->get(i))->m())*0.001);
		jet.push_back(j);
	}
	}
	}
		//Truth MET 
	for(auto met : *MET){
		mt.SetPxPyPzE(met->mpx()*0.001,met->mpy()*0.001,.0,met->sumet()*0.001);
		mtv.push_back(mt);
	}
		// Number of Photons 		
		hist("h_TruthPhotonN")->Fill(photon.size());
		// Number of Jets
		hist("h_TruthJetN")->Fill(jet.size());
		// Invariant mass of photons should == 125 H mass
		if(photon.size() == 2){
			hist("h_TruthPhotonInvM")->Fill((photon[0]+photon[1]).M());
			hist("h_TruthPhotonDeltaPhi")->Fill(fabs((photon[0]).DeltaPhi(photon[1])));	
			//hist("h_Ey")->Fill((photon[0].E()-ys[0].E())/ys[0].E());
			//hist("h_Ey")->Fill((photon[1].E()-ys[1].E())/ys[1].E());
		}
		if(ys.size() == 2){
			LS_truthphoton = LeadingAndSub(ys);
		}
		//Compute Di Higgs Pt;
		if(photon.size() == 2 && jet.size() == 2){
		ptg1 = photon[0].Pt(); ptg2 = photon[1].Pt(); ptb1 = jet[0].Pt(); ptb2 = jet[1].Pt();
		cg1 = cos(photon[0].Phi()); cg2 = cos(photon[1].Phi()); cb1 = cos(jet[0].Phi()); cb2 = cos(jet[1].Phi());
		sg1 = sin(photon[0].Phi()); sg2 = sin(photon[1].Phi()); sb1 = sin(jet[0].Phi()); sb2 = sin(jet[1].Phi());
		dptc = sqrt( pow((((ptg1*cg1)+(ptg2*cg2))+((ptb1*cb1)+(ptb2*cb2))),2) + pow((((ptg1*sg1)+(ptg2*sg2))+((ptb1*sb1)+(ptb2*sb2))),2) );
		sysbb = jet[0] + jet[1];
		sysyy = photon[0] + photon[1];
		sysyybb = sysbb + sysyy;
		hist("h_Ptyy")->Fill(sysyy.Pt());
		hist("h_Ptbb")->Fill(sysbb.Pt());
		}
		
		for(int i = 0; i<alljets.size(); i++){
			test6 = test6 + alljets[i];	
		}
		if(jet.size() == 2 ){
			test6 = test6 - jet[0];
			test6 = test6 - jet[1];
		if(photon.size() == 2){
			hist("h_PxHHvsPxJet")->Fill((jet[0]+jet[1]+photon[0]+photon[1]).Px(),test6.Px());
		}}
		for(unsigned int i=0; i<alljets.size(); i++){
		for(unsigned int j=0; j<bs.size(); j++){
			float drij = bs[j].DeltaR(alljets[i]);
			if(drij > 1){
				jetnotcount.push_back(alljets[i]);
			}
		}}
		hist("h_TruthJetNoCountN")->Fill(jetnotcount.size());
		for(unsigned int i=0; i<jetnotcount.size(); i++){
			test5 = test5 + jetnotcount[i];
		}
		hist("h_TruthJetNoCountPt")->Fill(test5.Pt());
		int n = 0;	
		if(jet.size() == 2 ){
		if(jet[0].DeltaR(jet[1]) > 0.4){
			hist("h_TruthJetInvM")->Fill((jet[0]+jet[1]).M());
			float dr00 = bs[0].DeltaR(jet[0]);
			float dr01 = bs[1].DeltaR(jet[0]);
			hist("h_TruthJetBDeltaR")->Fill(dr00);
			hist("h_TruthJetBDeltaR")->Fill(dr01);
			if(dr00 < 0.4){	
				float dr11 = bs[1].DeltaR(jet[1]);
				hist("h_TruthJetBDeltaR")->Fill(dr11);
				if(dr11 <= 0.4){
					sjet.push_back(jet[0]);
					sjet.push_back(jet[1]);	
					float de00 = bs[0].E() - jet[0].E();
					float de11 = bs[1].E() - jet[1].E(); 
					hist("h_TruthJetBDeltaE")->Fill(de00);
					hist("h_TruthJetBDeltaE")->Fill(de11);
				}else{
					nsb.push_back(bs[1]);
					hist("h_TruthBQuarknSmtPt")->Fill((bs[1]).Pt());
					hist("h_TruthBQuarknSmtEta")->Fill((bs[1]).Eta());
					n++;
				}
			}else if(dr01 < 0.4){
				float dr10 = bs[0].DeltaR(jet[1]);
				hist("h_TruthJetBDeltaR")->Fill(dr10);
				if(dr10 <= 0.4){
					sjet.push_back(jet[0]);
					sjet.push_back(jet[1]);
					float de10 = bs[0].E() - jet[1].E();
					float de01 = bs[1].E() - jet[0].E();
					hist("h_TruthJetBDeltaE")->Fill(de10);
					hist("h_TruthJetBDeltaE")->Fill(de01);
				}else{
					nsb.push_back(bs[0]);
					hist("h_TruthBQuarknSmtPt")->Fill((bs[0]).Pt());
					hist("h_TruthBQuarknSmtEta")->Fill((bs[0]).Eta());
					n++;
				}
			}else{
				nsjet = sjet;
			}
		}}
		hist("h_NsmatchBQ")->Fill(n);
		hist("h_NsmatchJet")->Fill(sjet.size());
		if(sjet.size()==2 && photon.size() == 2){
		hist("h_DiH_Px")->Fill((test5+sjet[0]+sjet[1]+photon[0]+photon[1]).Px());
		hist("h_DiH_Jet_DeltaPhi")->Fill(test5.DeltaPhi(sjet[0]+sjet[1]+photon[0]+photon[1]));
		}
		int nmadd = 0;
		if(sjet.size() == 2){
		if(sjet[0].DeltaR(sjet[1]) > 0.4){
			hist("h_TruthMJetMass")->Fill(sjet[0].M());
			hist("h_TruthMJetMass")->Fill(sjet[1].M());
			hist("h_TruthSmJetInvM")->Fill((sjet[0]+sjet[1]).M());
			sm0 = sjet[0],sm1 = sjet[1];
			for(unsigned int i = 0; i<tmuon.size(); i++){
				float dr0m = sjet[0].DeltaR(tmuon[i]);
				float dr1m = sjet[1].DeltaR(tmuon[i]);
				if(dr0m <= 0.4 && dr1m >= 0.4){
					sm0 = sm0 + tmuon[i];
					nmadd++;
				}else if (dr0m >= 0.4 && dr1m <= 0.4){
					sm1 = sm1 + tmuon[i];
					nmadd++;	
				}	
			}
			sum.push_back(sm0);
			sum.push_back(sm1);
		
			hist("h_NofMuonInCone")->Fill(sum.size());
			hist("h_NofMuonAddEvt")->Fill(nmadd);
		}}
		if(sum.size()==2){
		LS_truthjet = LeadingAndSub(sum);
		if(sum[0].DeltaR(sum[1]) > 0.4){
			hist("h_TruthJetInvAfMuonCorr")->Fill((sum[0]+sum[1]).M());
				summ = sum;
			for(unsigned k = 0; k<mtv.size(); k++){
				float drmet0 = sum[0].DeltaR(mtv[k]);
				float drmet1 = sum[1].DeltaR(mtv[k]);
					if(drmet0 <= 0.6){
						summ[0] = summ[0] + mtv[k];
					}else if(drmet1 <= 0.6){
						summ[1] = summ[1] + mtv[k];
					}
			}
			hist("h_TruthJetInvAfMETCorr")->Fill((summ[0]+summ[1]).M());
			//hist("h_Phi1")->Fill((sum[0].Phi() - bs[0].Phi())/bs[0].Phi());
			hist("h_TruthJetPt")->Fill(sum[0].Pt());
			hist("h_TruthJetPt")->Fill(sum[1].Pt());
			//hist("h_Phi2")->Fill((sum[1].Phi() - bs[1].Phi())/bs[1].Phi());
			hist("h_Pt1")->Fill((sum[1].Pt() - bs[1].Pt())/bs[1].Pt());
			hist("h_Pt1")->Fill((sum[0].Pt() - bs[0].Pt())/bs[0].Pt());
			hist("h_Pt2")->Fill((sum[0].Pt() - bs[0].Pt())/bs[0].Pt(),sum[0].Pt());
			hist("h_Pt2")->Fill((sum[1].Pt() - bs[1].Pt())/bs[1].Pt(),sum[1].Pt());
			hist("h_Pt3")->Fill(bs[0].Pt(),sum[0].Pt());
			hist("h_Pt3")->Fill(bs[1].Pt(),sum[1].Pt());
		}
		}
		
	//yybb systeme

	//yybb = ys[0]+ys[1]+bs[0]+bs[1];
	//hist("h_Ptyybb")->Fill(ys[0].Px()+ys[1].Px()+bs[0].Px()+bs[1].Px() + ys[0].Py()+ys[1].Py()+bs[0].Py()+bs[1].Py());
	// other jet 
		if(sum.size()==2){
		bb = bs[0]+bs[1];
		jj = sum[0]+sum[1];
		jjbb = jj - bb;
		int N = 0;
		for(unsigned int i = 0; i<alljets.size(); i++){
			float dr = jjbb.DeltaR(alljets[i]);
			if(dr < 0.4){
				N++;
			}
		}
		hist("h_N")->Fill(N);
		}
}
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++RECONSTRUCTED EVENT +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	for(unsigned int i = 0; i < recoElectronVector->size(); i++){
		//if(fabs((recoElectronVector->get(i))->eta()) < 2.5){
	 	bool isLHLoose = m_LooseLH->accept(recoElectronVector->get(i));
		if(isLHLoose){
		ele.SetPtEtaPhiE(((recoElectronVector->get(i))->pt())*0.001,((recoElectronVector->get(i))->eta()),((recoElectronVector->get(i))->phi()),((recoElectronVector->get(i))->e())*0.001);
		electron.push_back(ele);	
		}//}
	}	
	for( auto metjet : *RefMET){
		jmet.SetPxPyPzE(metjet->mpx()*0.001,metjet->mpy()*0.001,.0,metjet->sumet()*0.001);
		jetmet.push_back(jmet);
	}
	for(unsigned int i = 0; i < recoMuonVector->size(); i++){
		muon_quality = m_muonSelection.getQuality(*recoMuonVector->get(i));
		if(muon_quality == xAOD::Muon::Tight && ((recoMuonVector->get(i))->pt())*0.001 > 5){
		muon.SetPtEtaPhiE(((recoMuonVector->get(i))->pt())*0.001,((recoMuonVector->get(i))->eta()),((recoMuonVector->get(i))->phi()),((recoMuonVector->get(i))->e())*0.001);
		recomuon.push_back(muon);
		//std::cout<<"Muon with Medium Quality found!"<<std::endl;
	}
	}
	for(unsigned int i = 0; i < recoPhotonVector->size(); i++){
		float ptiso = 0.000001*(recoPhotonVector->get(i))->auxdata<float>("ptcone20");
		float etiso = 0.000001*(recoPhotonVector->get(i))->auxdata<float>("topoetcone20");
		float pt = (recoPhotonVector->get(i))->pt()*0.001;
		float et = (recoPhotonVector->get(i))->pt()*0.001;
		//if((recoPhotonVector->get(i))->auxdata<int>("truthOrigin") == 14){ 
		if(m_photonTight->accept(recoPhotonVector->get(i))){
		if(iso_tool->accept(*recoPhotonVector->get(i))){
		if((ptiso < 0.05*pt) && (etiso < 0.065*et)){
		if( fabs((recoPhotonVector->get(i))->eta()) < 1.37 || ( fabs((recoPhotonVector->get(i))->eta()) > 1.5 && fabs((recoPhotonVector->get(i))->eta()) < 2.37 ) ){
		ph.SetPtEtaPhiM(((recoPhotonVector->get(i))->pt())*0.001,((recoPhotonVector->get(i))->eta()),((recoPhotonVector->get(i))->phi()),0.0);
		recophoton.push_back(ph);
	}}}}}//}
		hist("h_RecoPhotonN")->Fill(recophoton.size());			
	
	if(recophoton.size() == 2){
		float myy = (recophoton[0] + recophoton[1]).M();
		float x0 = recophoton[0].Pt()/myy;
		float x1 = recophoton[1].Pt()/myy; 
		if((x1 > 0.35 && x0 > 0.25) || (x1 > 0.25 && x0 > 0.35)){
		if(myy<160 && myy > 105){
		//if((recophoton[0] + recophoton[1]).Pt() >= 150 ){
		LS_recophoton = LeadingAndSub(recophoton);
		hist("h_InvPhoton")->Fill(myy);
	}}}
		
	for(unsigned int i = 0; i < recoJetVector->size(); i++){
		bool isBTag = false;
		bool isBTagCalibrated = false;
		xAOD::Jet * calibratedjet = 0;
		float jvf = recoJetVector->get(i)->auxdata<float>("JvtJvfcorr");
		float jvt = recoJetVector->get(i)->auxdata<float>("Jvt");
		float jvtrpt = recoJetVector->get(i)->auxdata<float>("JvtRpt");
		float fabseta = fabs(recoJetVector->get(i)->eta());
		float pt = recoJetVector->get(i)->pt()*0.001; 
		hist("JVF")->Fill(jvf);
		hist("JVT")->Fill(jvt);
		hist("JVTRPT")->Fill(jvtrpt);
		hist("JVFvsRPT")->Fill(jvf,jvtrpt);
		if(jvf == 0){
			hist("eta0jvf")->Fill(recoJetVector->get(i)->eta());
		}
		//if(jvf > 0.9){
		//if(jvf != 0 && jvtrpt != 0){
		if((fabseta < 2.4 && pt > 20 && fabs(jvt) > 0.59) || (fabseta > 2.4 && pt > 30)){
		//if(jetsf->passesJvtCut(*(recoJetVector->get(i)))){
		if(((recoJetVector->get(i))->pt())*0.001 > 20 ){
		allrecojet.SetPtEtaPhiM(((recoJetVector->get(i))->pt())*0.001,((recoJetVector->get(i))->eta()),((recoJetVector->get(i))->phi()),((recoJetVector->get(i))->m())*0.001);
		allrecojets.push_back(allrecojet);
		hist("h_RecoJetPt")->Fill (((recoJetVector->get(i))->pt())*0.001); 
		hist("h_RecoJetM")->Fill (((recoJetVector->get(i))->m())*0.001); 
		hist("h_RecoJetEta")->Fill (((recoJetVector->get(i))->eta()));
		hist("h_RecoJetPhi")->Fill (((recoJetVector->get(i))->phi()));
		JetCalibrationTool_handle->calibratedCopy(*(recoJetVector->get(i)),calibratedjet); 
		isBTag = btagtool->accept(*(recoJetVector->get(i))); //Check BTagging 
		isBTagCalibrated = btagtool->accept(calibratedjet); //Check BTagging 
	//	}
		}
		}
		if(isBTag){
			bj.SetPtEtaPhiM(((recoJetVector->get(i))->pt())*0.001,((recoJetVector->get(i))->eta()),((recoJetVector->get(i))->phi()),((recoJetVector->get(i))->m())*0.001);
			bjet.push_back(bj);
			hist("h_RecoBJetPt")->Fill (((recoJetVector->get(i))->pt())*0.001); 
			hist("h_RecoBJetM")->Fill (((recoJetVector->get(i))->m())*0.001); 
			hist("h_RecoBJetEta")->Fill (((recoJetVector->get(i))->eta()));
			hist("h_RecoBJetPhi")->Fill (((recoJetVector->get(i))->phi())); 	
		}
		
		if(isBTagCalibrated){
			//JetCalibrationTool_handle->calibratedCopy(*(recoJetVector->get(i)),calibratedjet);
			bjcal.SetPtEtaPhiM((calibratedjet->pt())*0.001,(calibratedjet->eta()),(calibratedjet->phi()),(calibratedjet->m())*0.001);
			bjetcal.push_back(bjcal);
			hist("h_RecoBJetCalPt")->Fill ((calibratedjet->pt())*0.001); 
			hist("h_RecoBJetCalM")->Fill ((calibratedjet->m())*0.001); 
			hist("h_RecoBJetCalEta")->Fill ((calibratedjet->eta()));
			hist("h_RecoBJetCalPhi")->Fill ((calibratedjet->phi())); 	
		}
		
	}
		for( unsigned int i = 0; i<bjetcal.size(); i++){
		for( unsigned int j = 0; j<wzjet.size(); j++){
			float ptreco = wzjet[j].Pt()/bjetcal[i].Pt();
			hist("PtReco3")->Fill(bjetcal[i].Pt(),ptreco);			
		}
		} 
	// Matching B-tag and TruthWZ Jet 
		if(bjetcal.size() == 2 && wzjet.size() == 2){
		//for(unsigned int i = 0; i < wzjet.size(); i++){
			float dr00 = bjetcal[0].DeltaR(wzjet[0]); 
			float dr11 = bjetcal[1].DeltaR(wzjet[1]); 
			float dr01 = bjetcal[0].DeltaR(wzjet[1]); 
			float dr10 = bjetcal[1].DeltaR(wzjet[0]); 
			 if( dr00 < 0.4 && dr11 < 0.4){
				matchedwzjet.push_back(wzjet[0]);
				matchedwzjet.push_back(wzjet[1]);
			}
			if( dr10 < 0.4 && dr01 < 0.4){
				matchedwzjet.push_back(wzjet[0]);
				matchedwzjet.push_back(wzjet[1]);
			}
		}
		
		hist("h_NumberofmatchedWZBJet")->Fill(matchedwzjet.size());
		if(matchedwzjet.size() == 2) {hist("h_InvWZJet")->Fill( (matchedwzjet[0] + matchedwzjet[1]).M());LS_truthWZJet = LeadingAndSub(matchedwzjet);}
	// Add muon 
		int nmuon = 0, nelectron = 0;
		hist("h_RecoJetN")->Fill(allrecojets.size());
		hist("h_RecoBJetN")->Fill(bjet.size());
		hist("h_RecoBJetCalN")->Fill(bjetcal.size());
		if(bjet.size() == 2){
			hist("h_RecoNJet_additional")->Fill(allrecojets.size() - 2);
		}
		if(bjet.size() == 2 && LS_recophoton.size() == 2){
			allrecojets[0] = allrecojets[0] - bjet[0];
			allrecojets[1] = allrecojets[1] - bjet[1];
			//test6.clear();
			for(int i = 0; i<allrecojets.size(); i++){
				test6 = test6 + allrecojets[i];
			}
			hist("PxHHvsPxJet")->Fill((bjet[0]+bjet[1]+LS_recophoton[0]+LS_recophoton[1]).Px(),-test6.Px());
		}
		if(bjet.size() == 2){
		hist("h_InvJet")->Fill((bjet[0] + bjet[1]).M());
		//LS_recojet = LeadingAndSub(bjet);
		}
		if(bjetcal.size() == 2){
		hist("h_InvJetCal")->Fill((bjetcal[0] + bjetcal[1]).M());
		}
		if(bjetcal.size() == 2){
			LS_recojet = LeadingAndSubJet(bjetcal);
		}
		if(allrecojets.size() == 2 && bjetcal.size() == 2 && LS_recophoton.size() == 2 && (LS_recophoton[0]+LS_recophoton[1]).Pt() > 150 ){
			hist("h_mbb2jet2btagJES")->Fill((bjetcal[0]+bjetcal[1]).M());
		}
		if(allrecojets.size() == 3 && bjetcal.size() == 2 && LS_recophoton.size() == 2 && (LS_recophoton[0]+LS_recophoton[1]).Pt() > 150 ){
			hist("h_mbb3jet2btagJES")->Fill((bjetcal[0]+bjetcal[1]).M());
		}
		if(LS_recojet.size() == 2){
			bjet1pluselectron = LS_recojet[0];
			bjet2pluselectron = LS_recojet[1];
		for(unsigned int i = 0; i<electron.size(); i++){
			if(LS_recojet[0].DeltaR(electron[i]) < 0.4){
				bjet1pluselectron = bjet1pluselectron + electron[i];
				nelectron++;
			}else if(LS_recojet[1].DeltaR(electron[i]) < 0.4){
				bjet2pluselectron = bjet2pluselectron + electron[i];
				nelectron++;
			}
			LS_recojet[0] = bjet1pluselectron;
			LS_recojet[1] = bjet2pluselectron;
		}
		}
		hist("h_NumberElecAdd")->Fill(nelectron);
		if(LS_recojet.size() == 2){
			bjet1plusmuon = LS_recojet[0];
			bjet2plusmuon = LS_recojet[1];
		for(unsigned int i = 0; i<recomuon.size(); i++){
			if(LS_recojet[0].DeltaR(recomuon[i]) < 0.4){
				bjet1plusmuon = bjet1plusmuon + recomuon[i];
				nmuon++;
			}else if(LS_recojet[1].DeltaR(recomuon[i]) < 0.4){
				bjet2plusmuon = bjet2plusmuon + recomuon[i];
				nmuon++;
			}
			LS_recojet[0] = bjet1plusmuon;
			LS_recojet[1] = bjet2plusmuon;
		}
		}
		if(LS_recojet.size() == 2 && LS_recophoton.size() == 2){
			float px = (LS_recojet[0]+LS_recojet[1]+LS_recophoton[0]+LS_recophoton[1]).Px();
			float py = (LS_recojet[0]+LS_recojet[1]+LS_recophoton[0]+LS_recophoton[1]).Py();
			test.SetPxPyPzE(px,py,0.,0.);
		}
		if(bs.size() == 2){
		LS_truthQuark = LeadingAndSub(bs);}
		if(LS_truthjet.size() == 2 && LS_truthQuark.size() == 2){
		hist("h_PtQvsPttruth")->Fill(LS_truthQuark[0].Pt(),(LS_truthjet[0].Pt() - LS_truthQuark[0].Pt())/LS_truthQuark[0].Pt());
		hist("h_PtQvsPttruth")->Fill(LS_truthQuark[1].Pt(),(LS_truthjet[1].Pt() - LS_truthQuark[1].Pt())/LS_truthQuark[1].Pt());
		}
		if(LS_recojet.size() == 2 && LS_truthQuark.size() == 2){
		hist("h_PtQvsPtreco")->Fill(LS_recojet[0].Pt(),(LS_recojet[0].Pt() - LS_truthQuark[0].Pt())/LS_truthQuark[0].Pt());
		hist("h_PtQvsPtreco")->Fill(LS_recojet[1].Pt(),(LS_recojet[1].Pt() - LS_truthQuark[1].Pt())/LS_truthQuark[1].Pt());
		hist("h_PtQvsPtreco2")->Fill(LS_truthQuark[0].Pt(),(LS_recojet[0].Pt() - LS_truthQuark[0].Pt())/LS_truthQuark[0].Pt());
		hist("h_PtQvsPtreco2")->Fill(LS_truthQuark[1].Pt(),(LS_recojet[1].Pt() - LS_truthQuark[1].Pt())/LS_truthQuark[1].Pt());
		float ptrecoquark0 = LS_truthQuark[0].Pt()/LS_recojet[0].Pt();
		float ptrecoquark1 = LS_truthQuark[1].Pt()/LS_recojet[1].Pt();
		hist("PtReco4")->Fill(LS_recojet[0].Pt(),ptrecoquark0);
		hist("PtReco4")->Fill(LS_recojet[1].Pt(),ptrecoquark1);
		}
		if(LS_recojet.size() == 2){
		hist("h_InvJetAfterMuon")->Fill((LS_recojet[0] + LS_recojet[1]).M());
		}
		if(allrecojets.size() == 2 && LS_recojet.size() == 2 && LS_recophoton.size() == 2 && (LS_recophoton[0]+LS_recophoton[1]).Pt() > 150 ){
			hist("h_mbb2jet2btagMuon")->Fill((LS_recojet[0]+LS_recojet[1]).M());
		}
		if(allrecojets.size() == 3 && LS_recojet.size() == 2 && LS_recophoton.size() == 2 && (LS_recophoton[0]+LS_recophoton[1]).Pt() > 150 ){
			hist("h_mbb3jet2btagMuon")->Fill((LS_recojet[0]+LS_recojet[1]).M());
		}
		hist("h_NumberMuonAdd")->Fill(nmuon);
		if(LS_recojet.size() == 2 && LS_truthWZJet.size() == 2){
			float ptreco0 = LS_truthWZJet[0].Pt()/LS_recojet[0].Pt();
			float ptreco1 = LS_truthWZJet[1].Pt()/LS_recojet[1].Pt();
			hist("PtReco0")->Fill(LS_recojet[0].Pt(),ptreco0);
			hist("PtReco0")->Fill(LS_recojet[1].Pt(),ptreco1);
			hist("PtReco1")->Fill(LS_recojet[0].Pt(),ptreco0);
			hist("PtReco1")->Fill(LS_recojet[1].Pt(),ptreco1);
		}
		if(LS_recojet.size() == 2){
		for(unsigned int i = 0; i < LS_recojet.size(); i++){
			//float factor = hist("PtReco4")->Interpolate(LS_recojet[i].Pt());
			float factor = PtReco->Interpolate(LS_recojet[i].Pt());			
			hist("h_factor")->Fill(factor);
			test2 = LS_recojet[i]*factor;
			scaledjet.push_back(test2);
		}}

		if(scaledjet.size() == 2){
			hist("h_InvAfterPtReco")->Fill((scaledjet[0]+scaledjet[1]).M());
			LS_recojet = scaledjet;
			hist("mass")->Fill((scaledjet[0]+scaledjet[1]).M(),125.09/(scaledjet[0]+scaledjet[1]).M());
		}
		if(LS_recojet.size() == 2){
			float factor;
			float mass = (LS_recojet[0]+LS_recojet[1]).M();
			if(mass < 80){
				factor = 1.56;
			}else if(mass > 80 && mass < 110){
				factor = 1.34;
			}else if(mass > 110 && mass < 130){
				factor = 1.05;
			}else if(mass > 130 && mass < 150){
				factor = 0.9;
			}else if(mass > 150){
				factor = 0.8;
			}
			hist("h_mbb_sm")->Fill(mass*factor);
		}
		if(allrecojets.size() == 2 && LS_recojet.size() == 2 && LS_recophoton.size() == 2 && (LS_recophoton[0]+LS_recophoton[1]).Pt() > 150 ){
			hist("h_mbb2jet2btagPtReco")->Fill((LS_recojet[0]+LS_recojet[1]).M());
		}
		if(allrecojets.size() == 3 && LS_recojet.size() == 2 && LS_recophoton.size() == 2 && (LS_recophoton[0]+LS_recophoton[1]).Pt() > 150 ){
			hist("h_mbb3jet2btagPtReco")->Fill((LS_recojet[0]+LS_recojet[1]).M());
		}
		if(scaledjet.size() == 2 && LS_recophoton.size() == 2){
			hist("h_RecoDiH_Px")->Fill((scaledjet[0]+scaledjet[1]+LS_recophoton[0]+LS_recophoton[1]).Px());
			hist("h_RecoDiH_Py")->Fill((scaledjet[0]+scaledjet[1]+LS_recophoton[0]+LS_recophoton[1]).Py());
			hist("h_RecoDiH_Pt")->Fill((scaledjet[0]+scaledjet[1]+LS_recophoton[0]+LS_recophoton[1]).Pt());
		}
		if(scaledjet.size() == 2 && LS_truthphoton.size() == 2){
			hist("h_DemiRecoDiH_Px")->Fill((scaledjet[0]+scaledjet[1]+LS_truthphoton[0]+LS_truthphoton[1]).Px());
			hist("h_DemiRecoDiH_Py")->Fill((scaledjet[0]+scaledjet[1]+LS_truthphoton[0]+LS_truthphoton[1]).Py());
		}
		if(LS_recojet.size() == 2 && LS_truthjet.size() == 2){
			float ptreco0 = LS_truthjet[0].Pt()/LS_recojet[0].Pt();
			float ptreco1 = LS_truthjet[1].Pt()/LS_recojet[1].Pt();
			//if(nmuon == 0){
			hist("PtReco2")->Fill(LS_recojet[0].Pt(),ptreco0);
			hist("PtReco2")->Fill(LS_recojet[1].Pt(),ptreco1);
		for(unsigned int i = 0; i < LS_recojet.size(); i++){
			float factor2 = hist("PtReco2")->Interpolate(LS_recojet[i].Pt());
			test3 = LS_recojet[i]*factor2;
			//scaledjet2.push_back(test3);
		}
		} 
		if(LS_recojet.size() == 2){
		for(unsigned int i = 0; i < LS_recojet.size(); i++){
			double factor3;
			//factor3 = getscalefactor(LS_recojet[i].Pt());          
			//test4 = LS_recojet[i]*factor3;
			factor3 = PtReco->Interpolate(LS_recojet[i].Pt());
			test4 = LS_recojet[i]*factor3;
			hist("h_factor2")->Fill(factor3);
			scaledjet3.push_back(test4);
		}}
		if(scaledjet3.size() == 2){
			hist("h_InvAfterPtReco3")->Fill((scaledjet3[0]+scaledjet3[1]).M());
		}
		if(LS_recojet.size() == 2 && bs.size() == 2){
			hist("PtEtaJet")->Fill(LS_recojet[0].Pt(),LS_recojet[0].Eta());
			hist("PtEtaJet1")->Fill(LS_recojet[1].Pt(),LS_recojet[1].Eta());
			hist("h_Phi1")->Fill((LS_recojet[0].Phi() - bs[0].Phi())/bs[0].Phi());
			hist("h_Phi2")->Fill((LS_recojet[1].Phi() - bs[1].Phi())/bs[1].Phi());
		}
		if(scaledjet2.size() != 0){
			//hist("h_InvAfterPtReco2")->Fill((scaledjet2[0]+scaledjet2[1]).M());
			hist("h_InvAfterPtReco2")->Fill((scaledjet2[0]).M());
		}
		
		if(LS_truthjet.size() == 2 && LS_truthWZJet.size() == 2){
			hist("h_PtWZvsPttruth")->Fill(LS_truthjet[0].Pt() - LS_truthWZJet[0].Pt());
			hist("h_PtWZvsPttruth")->Fill(LS_truthjet[1].Pt() - LS_truthWZJet[1].Pt());
		}
	
	//if(LS_recojet.size() == 2 && LS_recophoton.size() == 2 ){//LS_truthjet.size() == 2 && LS_truthphoton.size() == 2 && LS_recophoton.size() == 2 && LS_truthQuark.size() == 2){
		//truth jet
		if(LS_recojet.size() == 2 && LS_truthjet.size() == 2){
		hist("h_PtRJ")->Fill((LS_recojet[0].Pt() - LS_truthjet[0].Pt())/LS_truthjet[0].Pt());
		hist("h_PtRJ2")->Fill((LS_recojet[1].Pt() - LS_truthjet[1].Pt())/LS_truthjet[1].Pt());
		hist("h_PtR2")->Fill((LS_recojet[0].Pt() - LS_truthjet[0].Pt())/LS_truthjet[0].Pt());
		hist("h_PtR2")->Fill((LS_recojet[1].Pt() - LS_truthjet[1].Pt())/LS_truthjet[1].Pt());
		}
		//truth Quark
		if(LS_recojet.size() == 2 && LS_truthQuark.size() == 2){
		hist("h_PtRQ")->Fill((LS_recojet[0].Pt() - LS_truthQuark[0].Pt())/LS_truthQuark[0].Pt());
		hist("h_PtRQ2")->Fill((LS_recojet[1].Pt() - LS_truthQuark[1].Pt())/LS_truthQuark[1].Pt());
		hist("h_PtR")->Fill((LS_recojet[0].Pt() - LS_truthQuark[0].Pt()));
		hist("h_PtR1")->Fill((LS_recojet[1].Pt() - LS_truthQuark[1].Pt()));
		}
		//truthphoton 
		if(LS_truthphoton.size() == 2 && LS_recophoton.size() == 2 && ((LS_recophoton[0]+LS_recophoton[1]).Pt()) > 150){
		hist("h_PtRY")->Fill((LS_recophoton[0].Pt() - LS_truthphoton[0].Pt())/LS_truthphoton[0].Pt());
		hist("h_PtRY2")->Fill((LS_recophoton[1].Pt() - LS_truthphoton[1].Pt())/LS_truthphoton[1].Pt());
		hist("h_Phiy")->Fill((LS_truthphoton[0].Phi() - LS_recophoton[0].Phi())/LS_truthphoton[0].Phi());
		hist("h_Phiy")->Fill((LS_truthphoton[1].Phi() - LS_recophoton[1].Phi())/LS_truthphoton[1].Phi());
		}
		if(LS_truthjet.size() == 2 && LS_recojet.size() == 2){
		hist("h_Phib")->Fill((LS_truthjet[0].Phi() - LS_recojet[0].Phi())/LS_truthjet[0].Phi());
		hist("h_Phib")->Fill((LS_truthjet[1].Phi() - LS_recojet[1].Phi())/LS_truthjet[1].Phi());
		}
		if(LS_truthphoton.size() == 2 && LS_recophoton.size() == 2 && ((LS_recophoton[0]+LS_recophoton[1]).Pt()) > 150){
		hist("h_Etab")->Fill((LS_truthphoton[0].Eta() - LS_recophoton[0].Eta())/LS_truthphoton[0].Eta());
		hist("h_Etab")->Fill((LS_truthphoton[1].Eta() - LS_recophoton[1].Eta())/LS_truthphoton[1].Eta());
		}
		//hist("h_Pxyybb")->Fill((LS_recojet[0]+LS_recojet[1]+LS_recophoton[0]+LS_recophoton[1]).Px());
		if(LS_truthQuark.size() == 2 && LS_recojet.size() == 2){
		hist("h_RvsPtQ")->Fill(LS_truthQuark[0].Pt(), (LS_recojet[0].Pt() - LS_truthQuark[0].Pt())/LS_truthQuark[0].Pt());
		hist("h_RvsPtQ")->Fill(LS_truthQuark[1].Pt(), (LS_recojet[1].Pt() - LS_truthQuark[1].Pt())/LS_truthQuark[1].Pt());
		hist("h_RvsPtQ2")->Fill(LS_truthQuark[0].Pt(), (LS_recojet[0].Pt() + 0.0940*LS_recojet[0].Pt() - LS_truthQuark[0].Pt())/LS_truthQuark[0].Pt());
		hist("h_RvsPtQ2")->Fill(LS_truthQuark[1].Pt(), (LS_recojet[1].Pt() + 0.1471*LS_recojet[1].Pt() - LS_truthQuark[1].Pt())/LS_truthQuark[1].Pt());
		}
		if(LS_recojet.size() == 2 && LS_recophoton.size() == 2 && ((LS_recophoton[0]+LS_recophoton[1]).Pt()) > 150 ){
		//hist("h_TruthDiH_PxAfter")->Fill(pxhh);
		//hist("h_TruthDiH_PyAfter")->Fill(pyhh);
		//hist("h_AfterDiH_Pt")->Fill(pthh);
		}
	//}
//Fill NTuple 
		if(LS_recojet.size() == 2 && LS_truthQuark.size() == 2){
		float pt0 = LS_recojet[0].Pt();
		float pt1 = LS_recojet[1].Pt();
		if(pt0 < 40){
			hist("h_ptresponce040")->Fill(LS_truthQuark[0].Pt()/LS_recojet[0].Pt());		
		}else if(pt0 > 40 && pt0 < 50){	
			hist("h_ptresponce4050")->Fill(LS_truthQuark[0].Pt()/LS_recojet[0].Pt());
		}else if(pt0 > 50 && pt0 < 60){
			hist("h_ptresponce5060")->Fill(LS_truthQuark[0].Pt()/LS_recojet[0].Pt());
		}else if(pt0 > 60 && pt0 < 70){
			hist("h_ptresponce6070")->Fill(LS_truthQuark[0].Pt()/LS_recojet[0].Pt());
		}else if(pt0 > 70 && pt0 < 80){
			hist("h_ptresponce7080")->Fill(LS_truthQuark[0].Pt()/LS_recojet[0].Pt());
		}else if(pt0 > 80 && pt0 < 100){
			hist("h_ptresponce80100")->Fill(LS_truthQuark[0].Pt()/LS_recojet[0].Pt());
		}else{
			hist("h_ptresponce100")->Fill(LS_truthQuark[0].Pt()/LS_recojet[0].Pt());	
		}		
		if(pt1 < 40){
			hist("h_ptresponce040")->Fill(LS_truthQuark[1].Pt()/LS_recojet[1].Pt());		
		}else if(pt1 > 40 && pt1 < 50){	
			hist("h_ptresponce4050")->Fill(LS_truthQuark[1].Pt()/LS_recojet[1].Pt());
		}else if(pt1 > 50 && pt1 < 60){
			hist("h_ptresponce5060")->Fill(LS_truthQuark[1].Pt()/LS_recojet[1].Pt());
		}else if(pt1 > 60 && pt1 < 70){
			hist("h_ptresponce6070")->Fill(LS_truthQuark[1].Pt()/LS_recojet[1].Pt());
		}else if(pt1 > 70 && pt1 < 80){
			hist("h_ptresponce7080")->Fill(LS_truthQuark[1].Pt()/LS_recojet[1].Pt());
		}else if(pt1 > 80 && pt1 < 100){
			hist("h_ptresponce80100")->Fill(LS_truthQuark[1].Pt()/LS_recojet[1].Pt());
		}else{
			hist("h_ptresponce100")->Fill(LS_truthQuark[1].Pt()/LS_recojet[1].Pt());	
		}													
		}
		if(LS_recophoton.size() == 2){
			hist("h_InvPhoton")->Fill((LS_recophoton[0] + LS_recophoton[1]).M());
			hist("h_RecoDiy_Pt")->Fill((LS_recophoton[0] + LS_recophoton[1]).Pt());
			hist("h_RecoDiy_Px")->Fill((LS_recophoton[0] + LS_recophoton[1]).Px());
			hist("h_RecoDiy_Py")->Fill((LS_recophoton[0] + LS_recophoton[1]).Py());
		}
		if(hs.size() == 2 && LS_recophoton.size() == 2 && scaledjet.size() == 2 && ys.size() == 2 && bs.size() == 2){
			hist("h_PxRvsPxTr")->Fill((hs[0]+hs[1]).Px(),(LS_recophoton[0]+LS_recophoton[1]+scaledjet[0]+scaledjet[1]).Px());
			hist("h_PxRvsPxTr2")->Fill((ys[0]+ys[1]).Px(),(LS_recophoton[0]+LS_recophoton[1]).Px());
			hist("h_PxRvsPxTr3")->Fill((bs[0]+bs[1]).Px(),(scaledjet[0]+scaledjet[1]).Px());
		}
		if(bs.size()==2 && ys.size() == 2){
			hist("h_PxRvsPxTr4")->Fill((bs[0]+bs[1]).Px(),-(ys[0]+ys[1]).Px());
		}  
//+++++++++++++++++++++++++++++ CUT Manager +++++++++++++++++++++++++++++++++++++++++++++
	if(scaledjet3.size() == 2 && LS_recophoton.size() == 2){
	float mbb = (scaledjet3[0]+scaledjet3[1]).M();
	float dr = scaledjet3[0].DeltaR(scaledjet3[1]);
	float deta = fabs(scaledjet3[0].Eta() - scaledjet3[1].Eta());
	float aco = 1-(fabs(scaledjet3[0].Phi()-scaledjet3[1].Phi())/pi);
	float myybb = (scaledjet3[0]+scaledjet3[1]+LS_recophoton[0]+LS_recophoton[1]).M();
	hist("DeltaRjj")->Fill(dr);
	if(dr < 1.8){
	hist("mbb")->Fill(mbb);
	if(mbb > 90 && mbb < 140){ 
	hist("myybb")->Fill(myybb);
	if(myybb > 350){
	hist("mbbAfterCut")->Fill(mbb);
	hist("DeltaEtajj")->Fill(deta);
	hist("Acojj")->Fill(aco);
	}
	}
	}
	}

		if(LS_recojet.size() == 2){
			LS_recojet = LeadingAndSub(LS_recojet);
		}
		if(LS_recophoton.size() == 2){
			LS_recophoton = LeadingAndSub(LS_recophoton);
		}
	if(allrecojets.size() == 3 && LS_recojet.size() == 2 && LS_recophoton.size() == 2 && (LS_recophoton[0]+LS_recophoton[1]).Pt() > 150){
		//Jet
		etaj1 = LS_recojet[0].Eta();
		etaj2 = LS_recojet[1].Eta();
		pttl = LS_recojet[0].Pt();
		pttsl = LS_recojet[1].Pt();
		ptl = LS_recojet[1].Pt();
		ptsl = LS_recojet[0].Pt();
		phib1 = LS_recojet[0].Phi();
		phib2 = LS_recojet[1].Phi();
		cphib1 = cos(phib1);
		cphib2 = cos(phib2);
		sphib1 = sin(phib1);
		sphib2 = sin(phib2);
		//Photon
		etay1 = LS_recophoton[0].Eta();
		etay2 = LS_recophoton[1].Eta();
		phiy1 = LS_recophoton[0].Phi();
		phiy2 = LS_recophoton[1].Phi();
		pty1 = LS_recophoton[0].Pt();
		pty2 = LS_recophoton[1].Pt();
		cphiy1 = cos(phiy1);
		cphiy2 = cos(phiy2);
		sphiy1 = sin(phiy1);
		sphiy2 = sin(phiy2);
		//Combinition
		pxyy = (LS_recophoton[0] + LS_recophoton[1]).Px();
		pyyy = (LS_recophoton[0] + LS_recophoton[1]).Py();
		ptyy = (LS_recophoton[0] + LS_recophoton[1]).Pt();
		dphibb = LS_recojet[0].DeltaPhi(LS_recojet[1]);
		dphiyy = LS_recophoton[0].DeltaPhi(LS_recophoton[1]);
		dphiyybb = (LS_recophoton[0] + LS_recophoton[1]).DeltaPhi(LS_recojet[0] + LS_recojet[1]);
		dphibbtruth = LS_recojet[0].DeltaPhi(LS_recojet[1]);
		detabb = LS_recojet[0].Eta() - LS_recojet[1].Eta();
		detayy = LS_recophoton[0].Eta() - LS_recophoton[1].Eta();
		detabbtruth = LS_recojet[0].Eta() - LS_recojet[1].Eta();
		pxpy = pxyy + pyyy;
		csphib1 = cphib1 + sphib1;
		csphib2 = cphib2 + sphib2;
	if (m_writeOutNTuple){
	    EL::NTupleSvc *ntuple = EL::getNTupleSvc (wk(), "output");
	    ntuple->tree()->Fill();
	  }
	  }//}	

  		//Clear all variables
		photon.clear();
		jet.clear();
		tparticle.clear();
		bs.clear();
		hs.clear();
		sjet.clear();
		nsb.clear();
		nsjet.size();
		tmuon.clear();
		sum.clear();
		mtv.clear();
		ys.clear();
		alljets.clear();
		allrecojets.clear();
		bjet.clear();
		summ.clear();
		LS_recojet.clear();
		LS_truthjet.clear();
		recophoton.clear();
		LS_recophoton.clear();
		scaledjet.clear();
		scaledjet2.clear();
		scaledjet3.clear();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  
  //xAOD::TEvent* event = wk()->xaodEvent();
   
  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  hist("PtReco0")->GetYaxis()->SetRangeUser(0.8,1.2);
  hist("PtReco1")->GetYaxis()->SetRangeUser(0.8,1.2);
  hist("PtReco2")->GetYaxis()->SetRangeUser(0.8,1.2);
  hist("PtReco4")->GetYaxis()->SetRangeUser(0.8,1.2);

  return EL::StatusCode::SUCCESS;
}
  std::vector<TLorentzVector> MyxAODAnalysis :: LeadingAndSub(std::vector<TLorentzVector> jet){
	std::vector<TLorentzVector> result;
	//float deltaphi = jet[0].DeltaPhi(jet[1]);
	//if(deltaphi > 140.){
		if(jet[0].Pt() > jet[1].Pt()){
			result.push_back(jet[0]);
			result.push_back(jet[1]);
		}
		if(jet[1].Pt() > jet[0].Pt()){
			result.push_back(jet[1]);
			result.push_back(jet[0]);
		}
	//}
		return result;	
}
  std::vector<TLorentzVector> MyxAODAnalysis :: LeadingAndSubJet(std::vector<TLorentzVector> jet){
	std::vector<TLorentzVector> result;
	//float deltaphi = jet[0].DeltaPhi(jet[1]);
	//if(deltaphi > 140.){
		if(jet[0].Pt() > jet[1].Pt() && jet[0].Pt() > 45 && jet[1].Pt() > 25){
			result.push_back(jet[0]);
			result.push_back(jet[1]);
		}
		if(jet[1].Pt() > jet[0].Pt() && jet[1].Pt() > 45 && jet[0].Pt() > 25){
			result.push_back(jet[1]);
			result.push_back(jet[0]);
		}
	//}
		return result;	
}
double MyxAODAnalysis :: getscalefactor(double pt){
	double factor;
	double p0 = 1.04749;
	double p1 = -0.000281919;
	double p2 = 3.39826e-07;
	double p3 = 5.49123e-06;
	double p4 = -4.85887e-08;
	double p5 = 2.6502e-10;
	double p6 = -8.95041e-13;
	double p7 = 1.81546e-15;
	double p8 = -2.01975e-18;
	double p9 = 9.44957e-22;
	factor = p0 + p1*pow(pt,1) + p2*pow(pt,2); //+ p3*pow(pt,3) + p4*pow(pt,4) + p5*pow(pt,5) + p6*pow(pt,6) + p7*pow(pt,7) + p8*pow(pt,8) + p9*pow(pt,9);
	if(pt<30){
	return 1;
	}else{
	return factor;
	}
}
