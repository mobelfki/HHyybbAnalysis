void ATestRun (const std::string& submitDir)
{
  //===========================================
  // FOR ROOT6 WE DO NOT PUT THIS LINE
  // (ROOT6 uses Cling instead of CINT)
  // Load the libraries for all packages
  // gROOT->Macro("$ROOTCOREDIR/scripts/load_packages.C");
  // Instead on command line do:
  // > root -l '$ROOTCOREDIR/scripts/load_packages.C' 'ATestRun.cxx ("submitDir")'
  // The above works for ROOT6 and ROOT5
  //==========================================
  
  
  // Set up the job for xAOD access:
  xAOD::Init().ignore();
  
  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;
  
  // scan for datasets in the given directory
  // this works if you are on lxplus, otherwise you'd want to copy over files
  // to your local machine and use a local path.  if you do so, make sure
  // that you copy all subdirectories and point this to the directory
  // containing all the files, not the subdirectories.
  
  /// Data samples
  std::string dataPath = "/eos/user/m/mobelfki/new/mc15_13TeV/";
 

  
  //SH::DiskListXRD xrdlist("eosatlas.cern.ch", dataPath);
  SH::ScanDir().scan(sh,dataPath);
  
  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");
  
  // further sample handler configuration may go here
  
  // print out the samples we found
  sh.print ();
  
  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  job.options()->setDouble (EL::Job::optMaxEvents, 60000); // for testing purposes, limit to run over the first 2000 events only!
  
  // Write plain output NTUPLE
  EL::OutputStream output  ("output");
  job.outputAdd (output);
  // configure ntuple object
  EL::NTupleSvc *ntuple = new EL::NTupleSvc ("output");
  ntuple->treeName("Fit");
  job.algsAdd (ntuple);

  //EL::NTupleSvc *ntupleData = new EL::NTupleSvc ("output");
  //ntuple->treeName("Data");
  //job.algsAdd (ntupleData);

  
  // add our algorithm to the job
  MyxAODAnalysis *alg = new MyxAODAnalysis;
  
  // later on we'll add some configuration options for our algorithm that go here
  alg->DoTruth(false);
  alg->DoReco(true);
  alg->PrintTruthChain(false);
  alg->WriteNTuple(true);
  
  job.algsAdd (alg);
  
  // make the driver we want to use:
  // this one works by running the algorithm directly:
  EL::DirectDriver driver;
  // we can use other drivers to run things on the Grid, with PROOF, etc.
  
  // process the job using the driver
  driver.submit (job, submitDir);
}
