#include "RooRealVar.h"
#include "RooAbsData.h"
#include "RooFormulaVar.h"
#include "RooDataHist.h"
#include "RooGaussian.h"
#include "RooDataSet.h"
#include "RooProdPdf.h"
#include "RooAddition.h"
#include "RooProduct.h"
#include "RooMinimizer.h"
#include "RooAbsReal.h"
#include "TH1.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "TTree.h"

 using namespace RooFit ;
void fit(){
  
RooRealVar ptj1("ptj1","Pt leading Jet",0,500);
RooRealVar ptj2("ptj2","Pt sub-leading Jet",0,500);
RooRealVar ptj3("ptj3","Pt Jet 3",0,500);
RooRealVar pty1("pty1","Pt leading reco photon",0,500);
RooRealVar pty2("pty2","Pt sub leading reco photon",0,500);
RooRealVar phij1("phij1","Phi leading reco jet",-3.2,3.2);
RooRealVar phij2("phij2","Phi sub leading reco jet",-3.2,3.2);
RooRealVar phiy1("phiy1","Phi leading reco photon",-3.2,3.2);
RooRealVar phiy2("phiy2","Phi sub leading reco photon",-3.2,3.2);
RooRealVar etaj1("etaj1","Eta leading Jet",-2.5,2.5);
RooRealVar etaj2("etaj2","Eta sub leading Jet",-2.5,2.5);
RooRealVar etay1("etay1","Eta leading Photon",-2.5,2.5);
RooRealVar etay2("etay2","Eta sub leading Photon",-2.5,2.5);
RooRealVar cphiy1("cphiy1","",-1.2,1.2);
RooRealVar cphiy2("cphiy2","",-1.2,1.2);
RooRealVar sphiy1("sphiy1","",-1.2,1.2);
RooRealVar sphiy2("sphiy2","",-1.2,1.2);
RooRealVar cphib1("cphib1","",-1.2,1.2);
RooRealVar cphib2("cphib2","",-1.2,1.2);
RooRealVar sphib1("sphib1","",-1.2,1.2);
RooRealVar sphib2("sphib2","",-1.2,1.2);
RooRealVar pxyy("pxyy","",-1000.,1000.);
RooRealVar pyyy("pyyy","",-1000.,1000.);
//RooRealVar pxj3("pxj3","",-1000.,1000.);
//RooRealVar pyj3("pyj3","",-1000.,1000.);
RooRealVar cphij3("cphij3","",-1.2,1.2);
RooRealVar sphij3("sphij3","",-1.2,1.2);
// Fit Variable //

RooRealVar ptj1fit("ptj1fit","Pt leading Jet After Fit",0,500);
RooRealVar ptj2fit("ptj2fit","Pt sub-leading Jet After Fit",0,500);
RooRealVar ptj3fit("ptj3fit","Pt Jet 3 After Fit",0,500);
RooRealVar pty1fit("pty1fit","Pt leading Photon After Fit",0,500);
RooRealVar pty2fit("pty2fit","Pt sub leading Photon After Fit",0,500);
RooRealVar phij1fit("phij1fit","Phi leading Jet After Fit ",-3.2,3.2);
RooRealVar phij2fit("phij2fit","Phi sub leading Jet After Fit",-3.2,3.2);
RooRealVar phiy1fit("phiy1fit","Phi leading Photon After Fit",-3.2,3.2);
RooRealVar phiy2fit("phiy2fit","Phi sub leading Photon After Fit",-3.2,3.2);
RooRealVar etaj1fit("etaj1fit","Eta leading Jet After Fit",-2.5,2.5);
RooRealVar etaj2fit("etaj2fit","Eta sub leading Jet After Fit",-2.5,2.5);
RooRealVar etay1fit("etay1fit","Eta leading Photon After Fit",-2.5,2.5);
RooRealVar etay2fit("etay2fit","Eta sub leading Photon After Fit",-2.5,2.5);

RooRealVar cphib1fit("cphib1fit","",-1.2,1.2);
RooRealVar cphib2fit("cphib2fit","",-1.2,1.2);
RooRealVar sphib1fit("sphib1fit","",-1.2,1.2);
RooRealVar sphib2fit("sphib2fit","",-1.2,1.2);


RooProduct fc1("fc1","",RooArgList(pty1fit,cphiy1));
RooProduct fc2("fc2","",RooArgList(pty2fit,cphiy2));
RooProduct fc3("fc3","",RooArgList(ptj1fit,cphib1));
RooProduct fc4("fc4","",RooArgList(ptj2fit,cphib2));
RooProduct fc5("fc5","",RooArgList(ptj3fit,cphij3));
RooAddition pxhh("pxhh_fit","Fitted",RooArgList(pxyy,fc3,fc4,fc5));

RooProduct fs1("fs1","",RooArgList(pty1fit,sphiy1));
RooProduct fs2("fs2","",RooArgList(pty2fit,sphiy2));
RooProduct fs3("fs3","",RooArgList(ptj1fit,sphib1));
RooProduct fs4("fs4","",RooArgList(ptj2fit,sphib2));
RooProduct fs5("fs5","",RooArgList(ptj3fit,sphij3));
RooAddition pyhh("pyhh_fit","Fitted",RooArgList(pyyy,fs3,fs4,fs5));

RooProduct fc12("fc12","",RooArgList(pty1,cphiy1));
RooProduct fc22("fc22","",RooArgList(pty2,cphiy2));
RooProduct fc32("fc32","",RooArgList(ptj1,cphib1));
RooProduct fc42("fc42","",RooArgList(ptj2,cphib2));
RooProduct fc52("fc52","",RooArgList(ptj3,cphij3));
RooAddition pxhh2("pxhh","before fit",RooArgList(pxyy,fc32,fc42,fc52));

RooProduct fs12("fs12","",RooArgList(pty1,sphiy1));
RooProduct fs22("fs22","",RooArgList(pty2,sphiy2));
RooProduct fs32("fs32","",RooArgList(ptj1,sphib1));
RooProduct fs42("fs42","",RooArgList(ptj2,sphib2));
RooProduct fs52("fs52","",RooArgList(ptj3,sphij3));
RooAddition pyhh2("pyhh","before fit",RooArgList(pyyy,fs32,fs42,fs52));


RooFormulaVar myy_fit("myy_fit","myy_fit","sqrt(2*pty1fit*pty2fit*(cosh(etay1-etay2)-cos(phiy1-phiy2)))",RooArgList(pty1fit,pty2fit,etay1,etay2,phiy1,phiy2));
RooFormulaVar mbb_fit("mbb_fit","mbb_fit","sqrt(2*ptj1fit*ptj2fit*(cosh(etaj1-etaj2)-cos(phij1-phij2)))",RooArgList(ptj1fit,ptj2fit,etaj1,etaj2,phij1,phij2));
RooFormulaVar myy("myy","myy","sqrt(2*pty1*pty2*(cosh(etay1-etay2)-cos(phiy1-phiy2)))",RooArgList(pty1,pty2,etay1,etay2,phiy1,phiy2));
RooFormulaVar mbb("mbb","mbb","sqrt(2*ptj1*ptj2*(cosh(etaj1-etaj2)-cos(phij1-phij2)))",RooArgList(ptj1,ptj2,etaj1,etaj2,phij1,phij2));
RooFormulaVar rep1("rep1","","ptj1fit/ptj1",RooArgList(ptj1fit,ptj1));
RooFormulaVar rep2("rep2","","ptj2fit/ptj2",RooArgList(ptj2fit,ptj2));
RooFormulaVar rep3("rep3","","ptj3fit/ptj3",RooArgList(ptj3fit,ptj3));


// Construction of Likelihood 

// LikeLihood
//RooProdPdf model("model","model",RooArgList(g1,g2,gausspxhh,gausspyhh),.0);

RooArgSet arg1(ptj1,ptj2,pty1,pty2,phij1,phij2,phiy1,phiy2);
arg1.add(RooArgSet(etaj1,etaj2,etay1,etay2,pxyy,pyyy,ptj3,cphij3,sphij3));
arg1.add(RooArgSet(sphib2,sphib1,cphib2,cphib1,sphiy2,sphiy1,cphiy2,cphiy1));

TFile *file = new TFile("mc15_13TeV.root");
TTree* tree = (TTree*) file->Get("Fit");
RooDataSet data("data","data",arg1,Import(*tree));

Double_t pt1,pt2,eta1,eta2,phi1,phi2,pxy,pyy,pxh,pyh,invm,ptb1,ptb2;

TTree *tree2 = new TTree("Fit","fit result");
	tree2->Branch("pt1",&pt1);
	tree2->Branch("pt2",&pt2);
	tree2->Branch("eta1",&eta1);
	tree2->Branch("eta2",&eta2);
	tree2->Branch("phi1",&phi1);
	tree2->Branch("phi2",&phi2);
	tree2->Branch("pxyy",&pxy);
	tree2->Branch("pyyy",&pyy);
	tree2->Branch("pxhh",&pxh);
	tree2->Branch("pyhh",&pyh);
	tree2->Branch("mbbfited", &invm);
	tree2->Branch("ptj1",&ptb1);	
	tree2->Branch("ptj2",&ptb2);		

//Integral Model 
//model.createProjection(arg1);
Int_t n = data.numEntries();
cout<<n<<endl;
// Fit
for( unsigned int ev = 0; ev < n; ev++){
cout<<" Event : " << ev+1 << endl;
TStopwatch t; t.Start();
RooArgSet event = *(data.get(ev));
RooDataSet data2("data2","data2",arg1);
data2.add(event);

RooRealVar* plot_ptj1 = (RooRealVar*)data2.get(0)->find("ptj1");
RooRealVar* plot_ptj2 = (RooRealVar*)data2.get(0)->find("ptj2");
RooRealVar* plot_ptj3 = (RooRealVar*)data2.get(0)->find("ptj3");
RooRealVar* plot_pty1 = (RooRealVar*)data2.get(0)->find("pty1");
RooRealVar* plot_pty2 = (RooRealVar*)data2.get(0)->find("pty2");

ptj1fit.setVal(100);
ptj2fit.setVal(100);
ptj3fit.setVal(100);
pty1fit.setVal(plot_pty1->getValV());
pty2fit.setVal(plot_pty2->getValV());

Double_t mn1,sig1,mn2,sig2,mn3,sig3;

transfer(plot_ptj1->getValV(),mn1,sig1);
transfer(plot_ptj2->getValV(),mn2,sig2);
transfer(plot_ptj3->getValV(),mn3,sig3);

RooRealVar moyenne1("moyenne1","",mn1);
RooRealVar ecarttype1("ecarttype1","",sig1);
RooRealVar moyenne2("moyenne2","",mn2);
RooRealVar ecarttype2("ecarttype2","",sig2);
RooRealVar moyenne3("moyenne3","",mn3);
RooRealVar ecarttype3("ecarttype3","",sig3);

RooRealVar meanpx("meanpxyy","",0.);
RooRealVar sigmapx("sigmapxyy","",30.);

//njet 0
RooGaussian gausspxhh("gausspxhh","",pxhh,meanpx,sigmapx);
RooGaussian gausspyhh("gausspyxhh","",pyhh,meanpx,sigmapx);

//njet 1
//RooGaussian gausspxhh("gausspxhh","",pxhh,pxj3,sigmapx);
//RooGaussian gausspyhh("gausspyxhh","",pyhh,pyj3,sigmapx);

RooGaussian gauss1("gauss1","",rep1,moyenne1,ecarttype1);
RooGaussian gauss2("gauss2","",rep2,moyenne2,ecarttype2);
RooGaussian gauss3("gauss3","",rep3,moyenne3,ecarttype3);

RooProdPdf model("model","model",RooArgList(gausspxhh,gausspyhh,gauss1,gauss2,gauss3),.0);

RooAbsReal* nll = model.createNLL(data2);
RooMinimizer m(*nll);
m.setErrorLevel(0.001);
m.migrad();
t.Stop(); t.Print();

data2.addColumn(ptj1fit);
data2.addColumn(ptj2fit);
data2.addColumn(ptj3fit);
data2.addColumn(pty1fit);
data2.addColumn(pty2fit);
data2.addColumn(pxhh);
data2.addColumn(pyhh);
data2.addColumn(pxhh2);
data2.addColumn(pyhh2);
data2.addColumn(myy);
data2.addColumn(mbb);
data2.addColumn(myy_fit);
data2.addColumn(mbb_fit);

data2.get(0)->Print("v");

RooRealVar* plot_etaj1 = (RooRealVar*)data2.get(0)->find("etaj1");
RooRealVar* plot_etaj2 = (RooRealVar*)data2.get(0)->find("etaj2");
RooRealVar* plot_phij1 = (RooRealVar*)data2.get(0)->find("phij1");
RooRealVar* plot_phij2 = (RooRealVar*)data2.get(0)->find("phij2");
RooRealVar* plot_pxyy = (RooRealVar*)data2.get(0)->find("pxyy");
RooRealVar* plot_pyyy = (RooRealVar*)data2.get(0)->find("pyyy");
RooRealVar* plot_invm = (RooRealVar*)data2.get(0)->find("mbb_fit");


pt1 = ptj1fit->getValV();
pt2 = ptj2fit->getValV();
pxh = pxhh->getValV();
pyh = pyhh->getValV();
eta1 = plot_etaj1->getVal();
eta2 = plot_etaj2->getVal();
phi1 = plot_phij1->getVal();
phi2 = plot_phij2->getVal();
pxy = plot_pxyy->getValV();
pyy = plot_pyyy->getValV();
invm = plot_invm->getValV();
ptb1 = plot_ptj1->getVal();
ptb2 = plot_ptj2->getVal();

tree2->Fill();
}

TFile *file2 = new TFile(TString::Format("reslut.root",ev),"RECREATE");
tree2->Write();
file2->Close();
}

void transfer(Double_t pt0,Double_t& mn,Double_t& sig){
	
	if(pt0 < 40 ){
		mn = 1.14;
		sig = 0.22;
	}else if(pt0 > 40 && pt0 < 50 ){
		mn = 1.12;
		sig = 0.18;
	}else if(pt0 > 50 && pt0 < 60){
		mn =  1.09;
		sig = 0.16;
	}else if(pt0 > 60 && pt0 < 70){
		mn = 1.06;
		sig = 0.16;
	}else if(pt0 > 70 && pt0 < 80){
		mn = 1.05;
		sig = 0.14;
	}else if(pt0 > 80 && pt0 < 100 ){
		mn = 1.05;
		sig = 0.13;
	}else if(pt0 > 100){
		mn = 1.03;
		sig = 0.10;
	}
}

